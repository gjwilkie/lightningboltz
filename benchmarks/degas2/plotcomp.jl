using DelimitedFiles
using JLD
using PyPlot
using GSL: sf_gamma

Lz = 2.0
Nz = 512
Nz_d2 = 128

dz= Lz/Nz
dz_d2= Lz/Nz_d2
zgrid = collect(range(0.5*dz,stop=Lz-0.5*dz,length=Nz))
zgrid_d2 = collect(range(0.5*dz_d2,stop=Lz-0.5*dz_d2,length=Nz_d2))

#data = readdlm("/Users/gwilkie/proj/gkyellcomp/LB/d2-ndensity.dat")
data = readdlm("d2-ndensity.dat")
d2_dens = data[:,2]
d2_err = data[:,3]
data = readdlm("d2-ntemperature.dat")
d2_temp = data[:,2]
d2_pres = d2_dens.*d2_temp
data = readdlm("d2-nflux.dat")
d2_flux = data[:,2]
d2_flow = data[:,2]./d2_dens

fout = load("output.jld")["fout"]
M = load("output.jld")["M"]
lb_dens= zeros(Nz)
for iz in 1:Nz
   psi_moments = M*fout[:,iz,end]
   lb_dens[iz] = sqrt(4*pi)*psi_moments[1]*sqrt(0.5*sf_gamma(1.5))
end

lb_dens = load("moments.jld")["dens"][:,end]
lb_flow = load("moments.jld")["flow"][:,3,end]
lb_temp = load("moments.jld")["temp"][:,end]
lb_pres = lb_dens.*lb_temp

      TiTe = 2.0
      delta = 0.1
      Te_up = 10.0*1.602e-19
      Ti_up = TiTe*Te_up
      Tw = 3.0*1.602e-19
      Lz=2.0

      Te = Tw .+ (Te_up-Tw)*tanh.( (1.0/delta)*(1.0 .- abs.( 1.0 .- 2.0*zgrid/Lz) ) )
      Ti = Tw .+ (Ti_up-Tw)*tanh.( (1.0/delta)*(1.0 .- abs.( 1.0 .- 2.0*zgrid/Lz) ) )
#

plt.figure(figsize=(11,6))
plt.subplot(231)
plt.title("Density")
#plt.xlabel("z (m)")
plt.ylabel(L"$n_n \,\, \left( \mathrm{m}^{-3} \right)$")
plt.semilogy(zgrid,lb_dens)
plt.semilogy(zgrid_d2,d2_dens,color="darkorange")
plt.legend(["LightningBoltz","DEGAS2"])
plt.subplot(232)
plt.title("Flux")
plt.semilogy(zgrid,lb_flow.*lb_dens)
plt.semilogy(zgrid_d2,d2_flux,color="darkorange")
plt.yscale("symlog",linthreshy=1.2e20)
#plt.xlabel("z (m)")
plt.ylabel(L"$\Gamma_{z,n} \,\,\left( \mathrm{m}^{-2} \mathrm{s}^{-1} \right)$")
plt.subplot(233)
plt.title("Pressure")
plt.semilogy(zgrid,lb_pres)
plt.semilogy(zgrid_d2,d2_pres*1.602e-19,color="darkorange")
#plt.xlabel("z (m)")
plt.ylabel(L"$p_n$ (Pa)")

plt.subplot(234)
plt.title("Density")
plt.xlabel("z (m)")
plt.ylabel(L"$n_n \,\, \left( \mathrm{m}^{-3} \right)$")
plt.plot(zgrid,lb_dens)
plt.plot(zgrid_d2,d2_dens,color="darkorange")
plt.plot(zgrid,5.0e19*ones(length(zgrid)),"--",color="gray")
plt.text(0.2,5.5e19,L"$n_e$",size=10,color="gray")
#plt.xlim([0.0,0.25])
plt.subplot(235)
plt.title("Velocity")
plt.plot(zgrid,lb_flow/1000.0)
plt.plot(zgrid_d2,d2_flow/1000.0,color="darkorange")
plt.plot(zgrid,zeros(length(zgrid)),"--",color="gray")
#plt.xlim([0.0,0.75])
plt.xlabel("z (m)")
plt.ylabel(L"$u_{z,n} \,\,\left( \mathrm{km} / \mathrm{s} \right)$")
plt.subplot(236)
plt.title("Temperature")
plt.plot(zgrid,lb_temp/1.602e-19)
plt.plot(zgrid_d2,d2_temp,color="darkorange")
plt.plot(zgrid,Te/1.602e-19,"--",color="gray")
plt.plot(zgrid,Ti/1.602e-19,"--",color="olivedrab")
#plt.xlim([0.0,0.75])
plt.text(0.08,5.0,L"$T_e$",size=10,color="gray")
plt.text(0.03,14.0,L"$T_i$",size=10,color="olivedrab")
plt.xlabel("z (m)")
plt.ylabel(L"$T_n$ (eV)")

plt.tight_layout()
plt.savefig("d2comp.pdf",bbox_inches="tight")
plt.clf()
plt.close()





