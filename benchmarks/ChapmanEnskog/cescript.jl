include("../../src/lightningboltz-core.jl")

p, outfile=  prepareParams("ce.json")

gradTarray = p.initTemp[1]*[2.0e-3,4.0e-3,6.0e-3,8.0e-3,0.01]

dz = p.Lz/p.Nz
zgrid = collect(range(0.5*dz,stop=p.Lz-0.5*dz,length=p.Nz))
NgradT = length(gradTarray)

dens = zeros(p.Nz,NgradT)
temp = zeros(p.Nz,NgradT)
temp_t = zeros(p.Nz,p.Nout)
hflux = zeros(p.Nz,NgradT)
hflux_t = zeros(p.Nz,p.Nout)
dens_t = zeros(p.Nz,p.Nout)
uz_t = zeros(p.Nz,p.Nout)
hflux_mid = zeros(NgradT)
gradT_mid = zeros(NgradT)

calculate_v_arrays(p)

for iT in 1:NgradT
   p.bndy_z_temp[1] = p.Tref-gradTarray[iT]*p.Lz*0.5
   p.bndy_z_temp[2] = p.Tref+gradTarray[iT]*p.Lz*0.5
   println("Boundary temps: $(p.bndy_z_temp[1]), $(p.bndy_z_temp[2])")

   if true
      t,fout, C, M, Minv = runCase(p)
      save("output_gradT_"*string(iT)*".jld","fout",fout,"t",t,"M",M,"Minv",Minv)
   else
      fout = load("output_gradT_"*string(iT)*".jld","fout")
      t = load("output_gradT_"*string(iT)*".jld","t")
      M = load("output_gradT_"*string(iT)*".jld","M")
      Minv = load("output_gradT_"*string(iT)*".jld","Minv")
   end

   for iz in 1:p.Nz
      n0,u0,T0,E0,M0,r0,s0,q0,dummy = calculate_moments(fout[:,iz,end],p)
      dens[iz,iT] = n0
      temp[iz,iT] = T0
      hflux[iz,iT] = q0
   end

   idx = round(Int64,p.Nz/2)
#   idx=12
   hflux_mid[iT] = hflux[idx,iT]
   gradT_mid[iT] = (temp[idx+1,iT]-temp[idx-1,iT])/(zgrid[idx+1]-zgrid[idx-1])

   for iout in 1:p.Nout
      for iz in 1:p.Nz
         n0,u0,T0,E0,M0,r0,s0,q0 = calculate_moments(fout[:,iz,iout],p)
         temp_t[iz,iout] = T0
         hflux_t[iz,iout] = q0
         uz_t[iz,iout] = u0[3]
         dens_t[iz,iout] = n0
      end
   end
   save("moments_gradT_"*string(iT)*".jld","temp_t",temp_t,"hflux_t",hflux_t,"dens_t",dens_t,"uz_t",uz_t)

   plot(zgrid,temp_t,"-o")
   legend(["it=1","it=2","it=3","it=4"])
   savefig("Tvsz"*string(iT)*".png")
   clf()
   close()

   plot(zgrid,dens_t,"-o")
   legend(["it=1","it=2","it=3","it=4"])
   savefig("nvsz"*string(iT)*".png")
   clf()
   close()

   plot(zgrid,hflux_t,"-o")
   legend(["it=1","it=2","it=3","it=4"])
   savefig("hfluxvsz"*string(iT)*".png")
   clf()
   close()
end

idx = round(Int64,p.Nz/2)
vref_array = sqrt.(temp/p.mass)
#chi_CE = 0.678*pi*vref[idx,:]./(dens[idx,:]*p.sigref)
chi_CE = 1.02513*(75.0/(64.0*sqrt(pi)))*pi*vref_array[idx,:]./(p.sigref)
 
plt.figure(figsize=(2.5,2.5))
plot(gradT_mid/1.602e-19,-hflux_mid,"o")
plot(gradT_mid/1.602e-19,chi_CE.*gradT_mid,"-")
println("LB: $(-hflux_mid)")
println("CE: $(chi_CE.*gradT_mid)")
#xlabel(L"$ T'(z)/T  (\mathrm{m}^{-1})$",fontsize=18)
xlabel(L"$ T'(z) \,\,\, (\mathrm{eV}/\mathrm{m})$",fontsize=18)
ylabel(L"$| q | (\mathrm{W}/\mathrm{m}^2)$",fontsize=18)
title("Heat flux",fontsize=18)
legend(["Simulation","Chapman-Enskog"])
savefig("hfluxVSgradT.pdf",bbox_inches="tight")
clf()
close()


Knarray = [0.01,0.02,0.05,0.1,0.2,0.5,1.0]

sigrefarray = (p.Lz*sum(p.initDens)*Knarray).^(-1)

NKn = length(Knarray)

chi = zeros(NKn)
chi_CE = zeros(NKn)
vref_array = sqrt.(temp[:,end]/p.mass)

for k in 1:NKn
   p.sigref = sigrefarray[k]

   if true
      t,fout, C, M, Minv = runCase(p)
      save("output_Kn_"*string(k)*".jld","fout",fout,"t",t,"M",M,"Minv",Minv)
   else
      fout = load("output_Kn_"*string(k)*".jld","fout")
      t = load("output_Kn_"*string(k)*".jld","t")
      M = load("output_Kn_"*string(k)*".jld","M")
      Minv = load("output_Kn_"*string(k)*".jld","Minv")
   end

   dens = zeros(p.Nz)
   temp = zeros(p.Nz)
   hflux = zeros(p.Nz)
   for iz in 1:p.Nz
      n0,u0,T0,E0,M0,r0,s0,q0 = calculate_moments(fout[:,iz,end],p)
      dens[iz] = n0
      temp[iz] = T0
      hflux[iz] = q0
   end

   idx = round(Int64,p.Nz/2)
#   idx = 4
chi[k] = -(hflux[idx]+hflux[idx+1])*(zgrid[idx+1]-zgrid[idx])/((temp[idx+1]-temp[idx])*(dens[idx+1]+dens[idx]))

   vref_array = sqrt.(temp/p.mass)

   chi_CE[k] = 1.02513*(75.0/(64.0*sqrt(pi)))*pi*vref_array[idx]./(dens[idx]*p.sigref)
 
end

println("chi_pred = ",chi)
println("chi_CE = ",chi_CE)


plt.figure(figsize=(2.5,2.5))
plot(Knarray,chi,"-o")
plot(Knarray,chi_CE,"--")
legend(["Simulation","Chapman-Enskog"])
xlabel("Kn")
ylabel(L"$\chi (\mathrm{m}^2/\mathrm{s})$")
savefig("chiVSKn.pdf",bbox_inches="tight")
clf()
close()


gradVarray = 5000.0*[2.0e-3,4.0e-3,6.0e-3,8.0e-3,0.01]
NgradV = length(gradVarray)

mflux = zeros(p.Nz,NgradV)
mflux_t = zeros(p.Nz,p.Nout)
ux = zeros(p.Nz,NgradV)
ux_t = zeros(p.Nz,p.Nout)
uz = zeros(p.Nz,NgradV)
uz_t = zeros(p.Nz,p.Nout)
dens = zeros(p.Nz,NgradV)
temp = zeros(p.Nz,NgradV)
mflux_mid = zeros(NgradV)
gradV_mid = zeros(NgradV)
Vx_mid = zeros(NgradV)
gradVz_mid = zeros(NgradV)

p.bndy_z_temp[1] = p.Tref
p.bndy_z_temp[2] = p.Tref
p.sigref = (p.Lz*sum(p.initDens)*0.01).^(-1)

for iV in 1:NgradV
   p.bndy_z_Vx[1] = -gradVarray[iV]*p.Lz*0.5
   p.bndy_z_Vx[2] =  gradVarray[iV]*p.Lz*0.5
   println("Boundary Vx: $(p.bndy_z_Vx[1]), $(p.bndy_z_Vx[2])")

   if true
      t,fout, C, M, Minv = runCase(p)
      save("output_gradV_"*string(iV)*".jld","fout",fout,"t",t,"M",M,"Minv",Minv)
   else
      fout = load("output_gradV_"*string(iV)*".jld","fout")
      t = load("output_gradV_"*string(iV)*".jld","t")
      M = load("output_gradV_"*string(iV)*".jld","M")
      Minv = load("output_gradV_"*string(iV)*".jld","Minv")
   end

   for iz in 1:p.Nz
      n0,u0,T0,E0,M0,r0,s0,q0,pxz = calculate_moments(fout[:,iz,end],p)
      dens[iz,iV] = n0
      temp[iz,iV] = T0
      ux[iz,iV] = u0[1]
      uz[iz,iV] = u0[3]
      mflux[iz,iV] = pxz
   end

   idx = round(Int64,p.Nz/2)
   mflux_mid[iV] = mflux[idx,iV] 
   gradV_mid[iV] = (ux[idx+1,iV]-ux[idx-1,iV])/(zgrid[idx+1]-zgrid[idx-1])
   gradVz_mid[iV] = (uz[idx+1,iV]-uz[idx-1,iV])/(zgrid[idx+1]-zgrid[idx-1])
   Vx_mid[iV] = ux[idx,iV]


   if true
   for iout in 1:p.Nout
      for iz in 1:p.Nz
         n0,u0,T0,E0,M0,r0,s0,q0,pxz = calculate_moments(fout[:,iz,iout],p)
         ux_t[iz,iout] = u0[1]
         mflux_t[iz,iout] = pxz
         uz_t[iz,iout] = u0[3]
         dens_t[iz,iout] = n0
      end
   end
   save("moments_gradV_"*string(iV)*".jld","temp_t",temp_t,"hflux_t",hflux_t,"dens_t",dens_t,"uz_t",uz_t)

   plot(zgrid,ux_t,"-o")
   legend(["it=1","it=2","it=3","it=4"])
   savefig("uxvsz"*string(iV)*".png")
   clf()
   close()

   plot(zgrid,uz_t,"-o")
   legend(["it=1","it=2","it=3","it=4"])
   savefig("uzvsz"*string(iV)*".png")
   clf()
   close()

   plot(zgrid,dens_t,"-o")
   legend(["it=1","it=2","it=3","it=4"])
   savefig("nvsz_gradv"*string(iV)*".png")
   clf()
   close()

   plot(zgrid,mflux_t,"-o")
   legend(["it=1","it=2","it=3","it=4"])
   savefig("mfluxvsz"*string(iV)*".png")
   clf()
   close()
   end

end

idx = round(Int64,p.Nz/2)
vref_array = sqrt.(temp/p.mass)
nu_CE = 1.016*(5.0/(16.0*sqrt(pi)))*pi*p.mass*vref_array[idx,:]/(p.sigref)

println("CE: $(nu_CE.*(gradV_mid))")
println("CE (proper): $(nu_CE.*(gradV_mid-(2.0/3.0*gradVz_mid)))")
println("LB: $(-mflux_mid)")

plt.figure(figsize=(2.5,2.5))
plot(gradV_mid,-mflux_mid,"o")
plot(gradV_mid,nu_CE.*(gradV_mid-(2.0/3.0*gradVz_mid)),"-")
#plot(gradV_mid,nu_CE.*(gradV_mid),"-")
xlabel(L"$ u_x'(z) \,\,\, (\mathrm{s}^{-1})$",fontsize=18)
ylabel(L"$\pi_{xz} \,\,\, \left( \mathrm{Pa} \right)$",fontsize=18)
title("Momentum flux ",fontsize=18)
legend(["Simulation","Chapman-Enskog"])
savefig("mfluxVSgradV.pdf",bbox_inches="tight")
clf()
close()


Knarray = [0.01,0.02,0.05,0.1,0.2,0.5,1.0]

sigrefarray = (p.Lz*sum(p.initDens)*Knarray).^(-1)

NKn = length(Knarray)

nu = zeros(NKn)
nu_CE = zeros(NKn)

for k in 1:NKn
   p.sigref = sigrefarray[k]

   if true
      t,fout, C, M, Minv = runCase(p)
      save("output_VKn_"*string(k)*".jld","fout",fout,"t",t,"M",M,"Minv",Minv)
   else
      fout = load("output_VKn_"*string(k)*".jld","fout")
      t = load("output_VKn_"*string(k)*".jld","t")
      M = load("output_VKn_"*string(k)*".jld","M")
      Minv = load("output_VKn_"*string(k)*".jld","Minv")
   end

   dens = zeros(p.Nz)
   ux = zeros(p.Nz)
   mflux = zeros(p.Nz)
   for iz in 1:p.Nz
      n0,u0,T0,E0,M0,r0,s0,q0,pxz = calculate_moments(fout[:,iz,end],p)
      dens[iz] = n0
      ux[iz] = u0[1]
      mflux[iz] = pxz
   end

   idx = round(Int64,p.Nz/2)
#   idx = 4
   nu[k] = -(mflux[idx]+mflux[idx+1])*(zgrid[idx+1]-zgrid[idx])/((ux[idx+1]-ux[idx])*p.mass*(dens[idx+1]+dens[idx]))

   vref = sqrt.(p.Tref/p.mass)

   nu_CE[k] = 1.016*(5.0/(16.0*sqrt(pi)))*pi*p.mass*vref/(p.sigref*dens[idx]*p.mass)
 
end

plt.figure(figsize=(2.5,2.5))
loglog(Knarray,nu,"-o")
loglog(Knarray,nu_CE,"--")
legend(["Simulation","Chapman-Enskog"])
xlabel("Kn")
ylabel(L"$\nu (\mathrm{m}^2/\mathrm{s})$")
savefig("nuVSKn.pdf",bbox_inches="tight")
clf()
close()




