include("../../src/lightningboltz-core.jl")

p, outfile = prepareParams("maxwmolec.json")

t,fout, C, M, Minv = runCase(p)

postprocess_0d(fout[:,1,:],p)
