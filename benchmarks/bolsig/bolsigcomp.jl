include("../../src/lightningboltz-core.jl")
using DelimitedFiles

p, outfile=  prepareParams("bolsig.json")

data = readdlm("bolsigresults.dat")
ENarray = data[:,1]
Eavg_bolsig = data[:,2]
mobility_bolsig = data[:,3]/p.initDens[1]

Earray = -ENarray*p.initDens[1]*1.0e-21
cond_bolsig = -p.initDens[1]*p.charge*mobility_bolsig

NE = length(Earray)

temp = zeros(NE)
temp_t = zeros(NE,p.Nout)
uz = zeros(NE)
uz_t = zeros(NE,p.Nout)

calculate_v_arrays(p)

for iE in 1:NE
   println(iE)
   p.Ez = -Earray[iE]

   if false
      t,fout, C, M, Minv = runCase(p)
      save("output_E_"*string(iE)*".jld","fout",fout,"t",t,"M",M,"Minv",Minv)
   else
      fout = load("output_E_"*string(iE)*".jld")["fout"]
      t = load("output_E_"*string(iE)*".jld")["t"]
      M = load("output_E_"*string(iE)*".jld")["M"]
      Minv = load("output_E_"*string(iE)*".jld")["Minv"]
   end

   for iout in 1:p.Nout
      n0,u0,T0,E0,M0,r0,s0,q0 = calculate_moments(fout[:,1,iout],p)
      temp_t[iE,iout] = T0
      uz_t[iE,iout] = u0[3]
   end
   temp[iE] = temp_t[iE,end]
   uz[iE] = uz_t[iE,end]

   save("moments_E_"*string(iE)*".jld","temp_t",temp_t,"uz_t",uz_t)

end

#sigma_LB = p.initDens[1]*p.charge*uz
#Eavg_LB = 1.5*temp/1.602e-19
# 
#plt.figure(figsize=(3,3))
#plot(Earray,Eavg_bolsig,"o")
#plot(Earray,Eavg_LB,"-")
#xlabel("Electric field (V/m)",fontsize=18)
#ylabel("Average electron energy (eV)",fontsize=18)
#legend(["BOLSIG+","LightningBoltz"])
#savefig("EavgVSEz.pdf",bbox_inches="tight")
#clf()
#close()
#
#plt.figure(figsize=(3,3))
#plot(Earray,sigma_bolsig,"o")
#plot(Earray,sigma_LB,"o")
#xlabel("Electric field (V/m)",fontsize=18)
#ylabel("Conductivity (A/V-m)",fontsize=18)
#legend(["BOLSIG+","LightningBoltz"])
#savefig("sigmaVSEz.pdf",bbox_inches="tight")
#

cond_LB = p.initDens[1]*p.charge*uz./Earray
Eavg_LB = (1.5*temp .+ 0.5*p.mass*uz.^2)/1.602e-19

plt.figure(figsize=(6,3))
plt.subplot(121)
plt.plot(-Earray,cond_LB,"-")
plt.plot(-Earray,cond_bolsig,"o",color="seagreen",markersize=3)
plt.title("Conductivity")
plt.xlabel("Electric field (V/m)")
plt.ylabel(L"$\sigma$ (A/V-m)")
plt.legend(["LightningBoltz","BOLSIG+"])
plt.subplot(122)
plt.plot(-Earray,Eavg_LB/3,"-")
plt.plot(-Earray,Eavg_bolsig/3,"o",color="seagreen",markersize=3)
plt.title("Temperature")
plt.xlabel("Electric field (V/m)")
plt.ylabel(L"$T_e$ (eV)")
#plt.legend(["BOLSIG+","LightningBoltz"])
plt.tight_layout()
plt.savefig("bolsigcomp.pdf",bbox_inches="tight")

clf()
close()

