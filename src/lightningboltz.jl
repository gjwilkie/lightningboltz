#!/usr/bin/env julia
# A fast spectral solver for the elastic Boltzmann equation 
# Written by George Wilkie (Chalmers U.), January 2018
# Implements the Galerkin-Petrov scheme of Gamba and Rjasanow 2017 ( https://arxiv.org/abs/1710.05903 )
# Lebedev quadrature rules provided by Casper Beentjes (http://people.maths.ox.ac.uk/beentjes/Essays/QuadratureSphere.pdf  )
# 1D spatial discretization by eigenfunction-decomposed upwinding (Kessler and Rjasanow 2018)

include("lightningboltz-core.jl")

if length(ARGS) > 0
   p, outfile=  prepareParams(ARGS[1])
else
   p, outfile=  prepareParams("test.json")
end

t,fout, C, M, Minv = runCase(p)

save("output.jld","fout",fout,"t",t,"M",M,"Minv",Minv)

if p.Nz == 1
   postprocess_0d(fout[:,1,:],p,M)
elseif p.Nz > 1
   postprocess_1d(fout,p,M)
end
