# Converts indices from (k=0:K,l=0:L,m=-L:L) to the array index i=1:n
@everywhere function get_idx(k::Int64,l::Int64,m::Int64,Lmax::Int64)
   # Number of m: 2l+1
   # Number of l: L+1
   # Number of k: K+1
   i = 1                # Start at index 1
   i += k*(Lmax+1)^2       # Account for k index
   for il in 0:l-1      # Account for l index. l=0, Add 0. l=1, add 1. l=2, add 1+3=4, etc.
      i += 2*il+1
   end
   i += m+l             # Account for m index
   return i
end

# Converts indices from the array index i=1:n to (k=0:K,l=0:L,m=-L:L)
@everywhere function get_idx(i::Int64,Lmax::Int64)
   k = div(i-1,(Lmax+1)^2) 
   ilm = (i-1)%((Lmax+1)^2)+1

   l=0
   m=0
   for il in 0:Lmax
      nm = 1+2*il
      if ilm - nm <= 0  # Found the right l
         l = il
         m = ilm-il-1
         break
      else 
         ilm = ilm - nm
      end
   end

   return k,l,m
end

# Intermediate matrix P_L for computing the collision Matrix
function getPL(theta::Vector,phi::Vector,w::Vector,p::paramData)
   NL = p.NSphere
   Lmax = p.NLegendre-1

   PL = zeros(Float64,p.NSphere,Lmax+1,2*Lmax+1)

   for l in 0:Lmax
      for m in -l:l
         for j in 1:p.NSphere
            PL[j,l+1,m+l+1] = w[j]*Ylm(l,m,theta[j],phi[j])
         end
      end
   end
   return PL
end

# Intermediate matrix P_GL for computing the collision Matrix
function getPGL(x_gm::Vector,w_gm::Vector,x_xs::Vector,w_xs::Vector,p::paramData)

   NGL = p.NGauss
   PGL = zeros(Float64,p.NGauss,p.NLaguerre,p.NLegendre)
   vref = sqrt(p.Tref/p.mass)

   x_use = deepcopy(x_gm)
   w_use = deepcopy(w_gm)
   if p.xsQuad
      x_use = deepcopy(x_xs)
      w_use = deepcopy(w_xs)
   end

   for k in 0:p.NLaguerre-1
      for l in 0:p.NLegendre-1
         mu_kl = basisnormfac(k,l)
         for i in 1:NGL
            x = x_use[i]/vref^2
            PGL[i,k+1,l+1] = mu_kl*w_use[i]*(x^(0.5*l))*sf_laguerre_n(k,l+0.5,x)
         end
      end
   end
   return PGL
end

# Intermediate matrix P_Q for computing the collision Matrix.
# This is intensive and so is parallelized.
function getPQ(x_gm::Vector,theta_leb::Vector,phi_leb::Vector,w_leb::Vector,p::paramData,icoll::Int64)
   global vsnorm, vs

   NL = p.NSphere
   NGL = p.NGauss
   Kmax = p.NLaguerre-1
   Lmax = p.NLegendre-1
   n = (Kmax+1)*(Lmax+1)^2
   
   vref = sqrt(p.Tref/p.mass)
#   @eval @everywhere vref = $vref
#
   @everywhere p = $p
   @everywhere global alphaParam
   @everywhere alphaParam = 1.0
   @everywhere global debyeParam
   @everywhere debyeParam = 0.0
   @everywhere global Tref
   @everywhere Tref = $(p.Tref)
   @everywhere icoll = $icoll
#   @eval @everywhere Tref = $Tref

   stationary = false
   linear_op = false

   collmodel = deepcopy(p.collModel[icoll])
   if p.collModel[icoll][end-3:end] == "_lin"
      collmodel = deepcopy(p.collModel[icoll][1:end-4])
      linear_op = true
   else
      collmodel = deepcopy(p.collModel[icoll][1:end])
      linear_op = false
   end


#   if occursin("_elec",collmodel)
#      stationary=true
#      linear_op=true
#      collmodel = deepcopy(collmodel[1:end-5])
#   end

   inelastic=false

   if collmodel == "maxwPseudo"
      kernelfunc = (v,w,theta,phi) -> kernel_maxwPseudo(v,w,theta,phi)
      alphafunc = (v,w) -> 1.0
   elseif collmodel == "hardsphere"
      kernelfunc = (v,w,theta,phi) -> kernel_hardsphere(v,w,theta,phi)
      alphafunc = (v,w) -> 1.0
   elseif collmodel == "hardsphere_elec"
      stationary=true
      linear_op=true
#      @eval @everywhere icoll=$icoll
      @everywhere kernelfunc = (v,w,theta,phi) -> kernel_hardsphere(v,w,theta,phi)
      @everywhere alphafunc = (v,w) -> 1.0
   elseif collmodel[1:6] == "lxcat_"
      stationary=true
      linear_op=true
      reaction_str = deepcopy(collmodel[7:end])
      filename = lxcatdir*"/"*reaction_str*".lxc"
      if occursin("diff",collmodel)
         @everywhere func = get_diff_lxcat_data(filename)
         @everywhere kernelfunc = (v,w,theta,phi) -> kernel_lxcat_diff(v,w,theta,phi,func)
      else
         func = get_lxcat_data(filename)
         kernelfunc = (v,w,theta,phi) -> kernel_lxcat(v,w,theta,phi,func)
      end
      if occursin("excite",collmodel)
         idx = findfirst("excite",collmodel)
         concat = collmodel[idx[end]+2:end]
         idx = findfirst("_",concat)
         eVloss_str = concat[1:idx[1]-1]
         eVloss = parse(Float64,eVloss_str)
         alphafunc = (v,w) -> getalpha( sum(v.^2) , sum(v.^2) , eVloss*1.602e-19,p.mass)
      else
         alphafunc = (v,w) -> 1.0
      end
   elseif collmodel[1:7] == "degas2_"
      reaction_str = deepcopy(collmodel[8:end])
      degas2dir="/Users/gwilkie/src/degas2/data"
      filename = degas2dir*"/"*reaction_str*".nc"
      func = get_degas2_data(filename,"cross_section")
      kernelfunc = (v,w,theta,phi) -> kernel_degas2(v,w,theta,phi,func)
      alphafunc = (v,w) -> 1.0
   elseif collmodel[1:7] == "coulomb"
      try debyeParam = parse(Float64,collmodel[9:end])
      catch
         error("Could not parse parameter from collModel = $(p.collModel[icoll])")
      end
      debyeParam = float(collmodel[9:end])
#      @eval @everywhere debyeParam = $debyeParam
      @everywhere kernelfunc = (v,w,theta,phi) -> kernel_coulomb(v,w,theta,phi,debyeParam)
      @everywhere alphafunc = (v,w) -> 1.0
   elseif collmodel == "chargeExchH"
      @everywhere kernelfunc = (v,w,theta,phi) -> kernel_chargeExchH(v,w,theta,phi,Tref)
#      @everywhere alphafunc = (v,w) -> getalpha_exch(v,w)
      @everywhere alphafunc = (v,w) -> 1.0
   elseif collmodel == "exciteH"
      @everywhere kernelfunc = (v,w,theta,phi) -> kernel_exciteH(v,w,theta,phi,Tref)
      @everywhere alphafunc = (v,w) -> getalpha( sum(v.^2)+sum(w.^2) , sum((v+w).^2) , 0.75*13.6/Tref)
   elseif collmodel[1:8] == "inelMaxw"
      try alphaParam = parse(Float64,collmodel[10:end])
      catch
         error("Could not parse parameter from collModel = $(p.collModel[icoll]). $(collmodel[10:end])")
      end
#      @eval @everywhere alphaParam = $alphaParam
      inelastic = true
      kernelfunc = kernel_maxwPseudo
      alphafunc = (v,w) -> alphaParam

   elseif collmodel[1:12] == "stickysphere"
      try alphaParam = float(collmodel[14:end])
      catch
         error("Could not parse parameter from collModel = $(p.collModel[icoll])")
      end
#      @eval @everywhere alphaParam = $alphaParam
      alphaParam = float(collmodel[14:end])
#      @eval @everywhere alphaParam = $alphaParam
      @everywhere kernelfunc = kernel_hardsphere
      @everywhere alphafunc = (v,w) -> alphaParam
   else
      error("Could not find collision kernel for collModel = $(p.collModel[icoll]) \n")
   end

   @everywhere vs_use = deepcopy(vsnorm)
   x_xs = deepcopy(x_gm)
   w_xs = deepcopy(w_gm)
   if p.xsQuad
      function g_xs(x::Float128)
         res = 0.5*sqrt(x)*exp(-0.5*x)*kernelfunc([convert(Float64,sqrt(x)*vref),0.0,0.0],[0.0,0.0,0.0],0.0,0.0)/(vref*p.sigref)
         return res
      end


      x_xs, w_xs = Genquad.genquadrules(g_xs,p.NGauss,[Float128(0.0),Float128(Inf)])
      for iv in 1:p.NGauss
         for jv in 1:p.NSphere
            vs_use[:,iv,jv] .= [sqrt(x_xs[iv]),theta_leb[jv],phi_leb[jv]]
         end
      end
      x_xs = x_xs*vref^2
      w_xs = w_xs*p.sigref*vref*vref^3
   end

   # Pre-compute psi(v/w) 
   psi_v = zeros(Float64,n,NGL,NL)
   for iv in 1:NGL
      for jv in 1:NL
         for i in 1:n
            k,l,m = get_idx(i,Lmax)
            psi_v[i,iv,jv] = test_function(vs_use[:,iv,jv],k,l,m)
         end
      end
   end


   prog = Progress(NL*NGL,0.5,"Calculating PQ array...")

   if stationary
      if length(p.targetMass) >= icoll
#         @eval @everywhere massratio = p.mass/(p.targetMass[icoll])
         massratio = p.mass/(p.targetMass[icoll])
      else
#         @eval @everywhere massratio = 0.0
         massratio = 0.0
      end

      PQ = zeros(Float64,NGL,NL,n)
      NLw = 1
      NGLw = 1
   else
      PQ = zeros(Float64,NGL,NL,NGL,NL,n)
      NLw = NL
      NGLw = NGL
   end
   PQw = SharedArray{Float64}(NGL,NL,n)
   for jw in 1:NLw
      for iw in 1:NGLw
         if stationary
            w = [0.0,0.0,0.0]
         else
            w = vs[:,iw,jw]
         end
         @sync @distributed for jv in 1:NL
#         for jv in 1:NL
            if jv == 1 
               PQw[:,:,:] .= 0.0
            end
            for iv in 1:NGL
               v = vs_use[:,iv,jv].*[vref,1.0,1.0]
               alpha = alphafunc(v,w)
               for j in 1:NL
                  theta = theta_leb[j]
                  phi = phi_leb[j]
                  if occursin("chargeExch",collmodel) || occursin("chex",collmodel) || occursin("chargex",collmodel)
                     vp = deepcopy(w)
                     wp = deepcopy(v)
                     linear_op=true
                  elseif stationary
                     vp = vprime_stationary(v,theta,phi,massratio,alpha)
                     wp = [0.0,0.0,0.0]
                  else
                     vp = vprime(v,w,theta,phi,alpha, 1.0)
                     wp = vprime(v,w,theta,phi,alpha,-1.0)
                  end

                  vpnorm = vp./[vref,1.0,1.0]
                  wpnorm = wp./[vref,1.0,1.0]
                  weight = w_leb[j]
                  if p.xsQuad
                     kernel = 1.0
                  else
                     kernel = kernelfunc(v,w,theta,phi)
                  end
                  if isnan(kernel)
                     error("Caught a NaN at j=$j,iv=$iv,jv=$jv,iw=$iw,jw=$jw")
                  end
                  for i in 1:n
                     k,l,m = get_idx(i,Lmax)

                     psisum = 0.0
                     psisum = psisum + test_function(vpnorm,k,l,m)
                     psisum = psisum - psi_v[i,iv,jv]
                     if !linear_op
                        psisum = psisum + test_function(wpnorm,k,l,m)
                        psisum = psisum - psi_v[i,iw,jw]
                        psisum = 0.5*psisum
                     end

                     PQw[iv,jv,i] += psisum * weight * kernel
                  end
               end
            end
         end
         @everywhere GC.gc()

         if stationary
            PQ[:,:,:] = PQw
         else
            PQ[:,:,iw,jw,:] = PQw
         end
         next!(prog)
      end
   end

   return PQ, x_xs, w_xs
end

@everywhere function getCollMatrixElement(k::Int64,l::Int64,i::Int64,NGL::Int64,NL::Int64,Lmax::Int64,n::Int64,PL::Array{Float64,3},PGL::Array{Float64,3},PQi::Array{Float64,4})

   kv,lv,mv = get_idx(k,Lmax)
   kw,lw,mw = get_idx(l,Lmax)
   tot = 0.0
   for iv in 1:NGL
      sum1 = 0.0
      for jv in 1:NL
         sum2 = 0.0
         for iw in 1:NGL
            innersum = 0.0
            for jw in 1:NL
               innersum += PL[jw,lw+1,mw+lw+1]*PQi[iv,jv,iw,jw]
            end
            sum2 += innersum*PGL[iw,kw+1,lw+1]
         end
         sum1 += sum2*PL[jv,lv+1,mv+lv+1]
      end
      tot += PGL[iv,kv+1,lv+1]*sum1
   end

   return tot
end

@everywhere function getCollMatrixElement(k::Int64,i::Int64,NGL::Int64,NL::Int64,Lmax::Int64,n::Int64,PL::Array{Float64,3},PGL::Array{Float64,3},PQi::Array{Float64,2})

   kv,lv,mv = get_idx(k,Lmax)
   tot = 0.0
   for iv in 1:NGL
      sum1 = 0.0
      for jv in 1:NL
         sum1 += PL[jv,lv+1,mv+lv+1] * PQi[iv,jv]
      end
      tot += PGL[iv,kv+1,lv+1]*sum1
   end

   # Used to be a mystery factor of 2*pi here.
   return tot
end

function buildStationaryCollMatrix(p::paramData,icoll::Int64)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1

   NGL = p.NGauss
   NL = p.NSphere

   C = zeros(Float64,n,n)
   if p.collModel[icoll] != "zero"
      PQ, x_xs, w_xs = getPQ(x_gm,theta_leb,phi_leb,w_leb,p,icoll)
      PGL = getPGL(x_gm,w_gm,x_xs,w_xs,p)
      PL = getPL(theta_leb,phi_leb,w_leb,p)

      prog = Progress(n,1.0,"Assembling collision matrix...")
      Qi = SharedArray{Float64}(n)
      for i in 1:n
         PQi = PQ[:,:,i]
         @sync @distributed for k in 1:n
            Qi[k] = getCollMatrixElement(k,i,NGL,NL,Lmax,n,PL,PGL,PQi)
         end
         @everywhere GC.gc()
         next!(prog)
   
         C[:,i] = Qi
      end
   end

   return C
end


function buildCollMatrix(p::paramData,icoll::Int64)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1

   NGL = p.NGauss
   NL = p.NSphere

   C = zeros(Float64,n,n,n)
   if p.collModel[icoll] != "zero"
      PQ, x_xs, w_xs = getPQ(x_gm,theta_leb,phi_leb,w_leb,p,icoll)
      PGL = getPGL(x_gm,w_gm,x_xs,w_xs,p)
      PL = getPL(theta_leb,phi_leb,w_leb,p)

      prog = Progress(n,1.0,"Assembling collision matrix...")
      Qi = SharedArray{Float64}(n,n)
      for i in 1:n
         PQi = PQ[:,:,:,:,i]
         @sync @distributed for k in 1:n
            for l in 1:n
               Qi[k,l] = getCollMatrixElement(k,l,i,NGL,NL,Lmax,n,PL,PGL,PQi)
            end
         end
         @everywhere GC.gc()
         next!(prog)
   
         C[:,:,i] = Qi
      end
   end

   return C
end

function buildMassMatrix(p::paramData)
   global x_gm, w_gm, theta_leb,phi_leb,w_leb

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1
   vref = sqrt(p.Tref/p.mass)

   nref = sum(p.initDens)

   M = zeros(Float64,n,n)
   E = zeros(Float64,n,n)
   Tz = zeros(Float64,n,n)

   for ki in 0:p.NLaguerre-1
      for kj in 0:p.NLaguerre-1
         for l in 0:Lmax
            for m in -l:l
               i = get_idx(ki,l,m,Lmax)
               j = get_idx(kj,l,m,Lmax)
               
               tot = 0.0
               for iv in 1:p.NGauss
                  x = x_gm[iv]/vref^2
                  Lkj = sf_laguerre_n(kj,l+0.5,x)
                  Lki = sf_laguerre_n(ki,l+0.5,x)
                  tot += w_gm[iv] * (x^(l)) * Lkj * Lki
               end
               M[i,j] = basisnormfac(kj,l)*basisnormfac(ki,l)*tot
            end
         end
      end
   end

   # Roundoff error makes M not exactly symmetric, but it ought to be. Force it here.
   M = 0.5*(M+transpose(M))

   EmatrixOption=2

# "Baseline". Basis function gets derivatives
   if EmatrixOption == 1
      if p.Ez != 0.0
   
         for i in 1:n
            ki, li, mi  = get_idx(i,Lmax)
            for j in 1:n
               kj, lj, mj  = get_idx(j,Lmax)
   
               if mj == mi 
                  m = mj
   
                  Iv1a = 0.0
                  Iv1b = 0.0
                  Iv2 = 0.0
   
                  if mod(li+lj+1+2*kj+2*ki ,2) == 0
                     vhat = sqrt.(x_gm)/vref
                     weights = deepcopy(w_gm)
                     lhold = 0
                  else
                     vhat = sqrt.(x_gl)/vref
                     weights = deepcopy(w_gl)
                     lhold = 1
                  end
                  for iv in 1:p.NGauss
   
                     Lki = sf_laguerre_n(ki,li+0.5,vhat[iv]^2)
                     Lkj = sf_laguerre_n(kj,lj+0.5,vhat[iv]^2)
                     Iv1a += -vhat[iv]^(lj+li+1+lhold)*Lki*Lkj*weights[iv]
                  end
   
                  if lj != 0
                     if mod(li-1+lj+2*kj+2*ki ,2) == 0
                        vhat = sqrt.(x_gm)/vref
                        weights = deepcopy(w_gm)
                        lhold = 0
                     else
                        vhat = sqrt.(x_gl)/vref
                        weights = deepcopy(w_gl)
                        lhold = 1
                     end
                     
                     for iv in 1:p.NGauss
   
                        Lki = sf_laguerre_n(ki,li+0.5,vhat[iv]^2)
                        Lkj = sf_laguerre_n(kj,lj+0.5,vhat[iv]^2)
                        Iv1a += lj*vhat[iv]^(lj+li-1+lhold)*Lki*Lkj*weights[iv]
                     end
                  end
   
                  if kj != 0
                     if mod(li+lj+1+2*kj+2*(ki-1) ,2) == 0
                        vhat = sqrt.(x_gm)/vref
                        weights = deepcopy(w_gm)
                        lhold = 0
                     else
                        vhat = sqrt.(x_gl)/vref
                        weights = deepcopy(w_gl)
                        lhold = 1
                     end
   
                     for iv in 1:p.NGauss
                        Lkjm1 = sf_laguerre_n(kj-1,lj+1.5,vhat[iv]^2)
                        Lki = sf_laguerre_n(ki,li+0.5,vhat[iv]^2)
                        Iv1b += -2.0*(vhat[iv]^(lj+li+1+lhold))*Lkjm1*Lki*weights[iv]
                     end
                  end
   
                  if mod(li+lj-1+2*ki+2*kj,2) == 0
                     vhat = sqrt.(x_gm)/vref
                     weights = deepcopy(w_gm)
                     lhold = 0
                  else
                     vhat = sqrt.(x_gl)/vref
                     weights = deepcopy(w_gl)
                     lhold = 1
                  end
   
                  for iv in 1:p.NGauss
                     Lki = sf_laguerre_n(ki,li+0.5,vhat[iv]^2)
                     Lkj = sf_laguerre_n(kj,lj+0.5,vhat[iv]^2)
                     Iv2 += (vhat[iv]^(lj+li-1+lhold))*Lki*Lkj*weights[iv]
                  end
   
                  fac1 = sqrt( (2*lj+1)*(lj+1+abs(mj))/( (2*lj+3)*(lj+1-abs(mj)) ) )
                  fac2 = sqrt( (2*lj+1)*(lj-abs(mj))/( (2*lj-1)*(lj+abs(mj)) ) )
   
                  if li == lj+1
                     Iom1 = ((lj-mj+1)/(2*lj+1))*fac1
                     Iom2 = -(lj*(lj-mj+1)/(2*lj+1))*fac1
                  elseif li == lj-1
                     Iom1 = ((lj+mj)/(2*lj+1))*fac2
                     Iom2 = (lj+1)*((lj+mj)/(2*lj+1))*fac2
                  else
                     Iom1 = 0.0
                     Iom2 = 0.0
                  end
   
                  EfieldElement = -basisnormfac(ki,li)*basisnormfac(kj,lj)*((Iv1a+Iv1b)*Iom1 + Iv2*Iom2)
   
                  if isnan(EfieldElement)
                     error("Efield element is NaN at element $i,$j. (k,l,m)_i = ($ki,$li,$mi). (k,l,m)_j = ($kj,$lj,$mj)")
                  end
   
   
                  E[i,j] = E[i,j] + (p.charge/p.mass)*(1.0/vref)*EfieldElement
               end 
   
            end
         end
      end
   end

   if EmatrixOption == 2
## After integrating by parts (test function gets derivatives)
      if p.Ez != 0.0
   
         for i in 1:n
            ki, li, mi  = get_idx(i,Lmax)
            for j in 1:n
               kj, lj, mj  = get_idx(j,Lmax)
   
               if mj == mi 
                  m = mj
   
                  Iv1a = 0.0
                  Iv1b = 0.0
                  Iv2 = 0.0
   
                  if li != 0
                     if mod(li-1+lj+2*kj+2*ki ,2) == 0
                        vhat = sqrt.(x_gm)/vref
                        weights = deepcopy(w_gm)
                        lhold = 0
                     else
                        vhat = sqrt.(x_gl)/vref
                        weights = deepcopy(w_gl)
                        lhold = 1
                     end
                     
                     for iv in 1:p.NGauss
   
                        Lki = sf_laguerre_n(ki,li+0.5,vhat[iv]^2)
                        Lkj = sf_laguerre_n(kj,lj+0.5,vhat[iv]^2)
                        Iv1a += li*vhat[iv]^(lj+li-1+lhold)*Lki*Lkj*weights[iv]
                     end
                  end
   
                  if ki != 0
                     if mod(li+lj+1+2*kj+2*(ki-1) ,2) == 0
                        vhat = sqrt.(x_gm)/vref
                        weights = deepcopy(w_gm)
                        lhold = 0
                     else
                        vhat = sqrt.(x_gl)/vref
                        weights = deepcopy(w_gl)
                        lhold = 1
                     end
   
                     for iv in 1:p.NGauss
                        Lkim1 = sf_laguerre_n(ki-1,li+1.5,vhat[iv]^2)
                        Lkj = sf_laguerre_n(kj,lj+0.5,vhat[iv]^2)
                        Iv1b += -2.0*(vhat[iv]^(lj+li+1+lhold))*Lkim1*Lkj*weights[iv]
                     end
                  end
   
                  if mod(li+lj-1+2*ki+2*kj,2) == 0
                     vhat = sqrt.(x_gm)/vref
                     weights = deepcopy(w_gm)
                     lhold = 0
                  else
                     vhat = sqrt.(x_gl)/vref
                     weights = deepcopy(w_gl)
                     lhold = 1
                  end
   
                  for iv in 1:p.NGauss
                     Lki = sf_laguerre_n(ki,li+0.5,vhat[iv]^2)
                     Lkj = sf_laguerre_n(kj,lj+0.5,vhat[iv]^2)
                     Iv2 += (vhat[iv]^(lj+li-1+lhold))*Lki*Lkj*weights[iv]
                  end
   
                  fac1 = sqrt( (2*li+1)*(li+1+abs(mi))/( (2*li+3)*(li+1-abs(mi)) ) )
                  fac2 = sqrt( (2*li+1)*(li-abs(mi))/( (2*li-1)*(li+abs(mi)) ) )
   
                  if lj == li+1
                     Iom1 = ((li-mi+1)/(2*li+1))*fac1
                     Iom2 = -(li*(li-mi+1)/(2*li+1))*fac1
                  elseif lj == li-1
                     Iom1 = ((li+mi)/(2*li+1))*fac2
                     Iom2 = (li+1)*((li+mi)/(2*li+1))*fac2
                  else
                     Iom1 = 0.0
                     Iom2 = 0.0
                  end
   
                  EfieldElement = -basisnormfac(ki,li)*basisnormfac(kj,lj)*((Iv1a+Iv1b)*Iom1 + Iv2*Iom2)
   
                  if isnan(EfieldElement)
                     error("Efield element is NaN at element $i,$j. (k,l,m)_i = ($ki,$li,$mi). (k,l,m)_j = ($kj,$lj,$mj)")
                  end
   
   
                  E[i,j] = E[i,j] + (p.charge/p.mass)*(1.0/vref)*EfieldElement
               end 
   
            end
         end
      end
   end

## Old way. 
#   if p.Ez != 0.0
#
#      for i in 1:n
#         ki, li, mi  = get_idx(i,Lmax)
#         for j in 1:n
#            kj, lj, mj  = get_idx(j,Lmax)
#
#            if mj == mi 
#
#               EfieldElement = 0.0
#               for iv in 1:p.NGauss
#
#                  x = x_gm[iv]
#                  v = sqrt(x)
#
#                  for jv in 1:p.NSphere
#
#                     v_vec = vsnorm[:,iv,jv]
#
#                     Lki = sf_laguerre_n(ki,li+0.5,x/vref^2)
#                     if ki == 0
#                        Lkim1lip1 = 0.0
#                     else
#                        Lkim1lip1 = sf_laguerre_n(ki-1,li+1.5,x/vref^2)
#                     end 
#    
#                     fac1 = sqrt( ((2.0*li)+1.0) * (li+abs(mi)+1.0) * (li-abs(mi)+1.0) / ((2.0*li)+3.0))*Ylm(li+1,mi,theta_leb[jv],phi_leb[jv]) - (li+1)*cos(theta_leb[jv])*Ylm(li,mi,theta_leb[jv],phi_leb[jv])
#
#                     bigfac = cos(theta_leb[jv])*Ylm(li,mi,theta_leb[jv],phi_leb[jv])*( li*(v/vref)^(li-1)*Lki - 2*((v/vref)^(li+1))*Lkim1lip1 ) - Lki*((v/vref)^(li-1))*fac1
#
#                     EfieldElement = EfieldElement + basis_function(v_vec,kj,lj,mj) * bigfac * exp(0.5*x/vref^2) * w_gm[iv]*w_leb[jv] *2.0*pi
#                  end
#               end
#
#               if isnan(EfieldElement)
#                  error("Efield element is NaN at element $i,$j. (k,l,m)_i = ($ki,$li,$mi). (k,l,m)_j = ($kj,$lj,$mj)")
#               end
#
#               E[i,j] = E[i,j] - (p.charge/p.mass)*EfieldElement/vref
#
#            end 
#
#         end
#      end
#   end

   if p.Nz > 1
      for i in 1:n
         ki,li,mi = get_idx(i,Lmax)
         for j in 1:n
            kj,lj,mj = get_idx(j,Lmax)
            
            if mi == mj
               m = mi
#               function Tintegrand(vnorm)
#                  return test_function(vnorm,ki,li,mi)*basis_function(vnorm,kj,lj,mj)*vnorm[1]*cos(vnorm[2])
#               end
#               Tz[i,j] = vref*integratevspace(Tintegrand) * vref^3
               if (li == lj + 1) #&& (li < Lmax)
                  if lj + 1 > abs(m)
                     fac = ((lj+m+1)/(2*lj+1))*sqrt( (2*lj+1)*(lj+1+abs(m)) / ( (2*lj+3)*(lj+1-abs(m))) )
                  else
                     fac = 0.0
                  end
                  tot = 0.0
                  for iv in 1:p.NGauss
                     x = x_gm[iv]/vref^2
                     Lki = sf_laguerre_n(ki,li+0.5,x)
                     Lkj = sf_laguerre_n(kj,lj+0.5,x)
                     tot += w_gm[iv] * (x^(0.5*(li+lj))) * Lki * Lkj*sqrt(x)*vref
                  end
                  Tz[i,j] += basisnormfac(kj,lj)*basisnormfac(ki,li)*fac*tot

               elseif li == lj - 1 #&& (li > 0)
                  if lj > abs(m)
                     fac = ((lj-m)/(2*lj+1))*sqrt( (2*lj+1)*(lj-abs(m)) / ( (2*lj-1)*(lj+abs(m))) )
                  else
                     fac = 0.0
                  end
                  tot = 0.0
                  for iv in 1:p.NGauss
                     x = x_gm[iv]/vref^2
                     Lki = sf_laguerre_n(ki,li+0.5,x)
                     Lkj = sf_laguerre_n(kj,lj+0.5,x)
                     tot += w_gm[iv] * (x^(0.5*(li+lj))) * Lki * Lkj *sqrt(x)*vref
                  end
                  Tz[i,j] += basisnormfac(kj,lj)*basisnormfac(ki,li)*fac*tot
               end
            end
         end
      end
   end

   return M, E, Tz
end

function decomposeTransportMatrix(T::Matrix,M::Matrix)
   U = eigvals(T,M)
   R = eigvecs(T,M)
   return U, R
end

# Generates a Maxwellian source at the given temperature.
function buildMaxwSource(p::paramData,Tsource::Float64)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb, vs,vsnorm,vc

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1
   vref = sqrt(p.Tref/p.mass)

   NGL = p.NGauss
   NL = p.NSphere

   sourceVector = expand_fM_local(p,1.0,[0.0,0.0,0.0],Tsource)
#   sourceVector = zeros(n)

#   for i in 1:n

#      k,l,m = get_idx(i,Lmax)
#      integral = 0.0
#      for iv in 1:p.NGauss
#         for jv in 1:p.NSphere
#            integral += w_gm[iv]*w_leb[jv]*test_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)*fM(vc[:,iv,jv],Tsource/p.mass)
#            integral += w_gm[iv]*w_leb[jv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)*fM(vc[:,iv,jv],Tsource/p.mass)
#         end
#      end
#      sourceVector[i] = integral
#   end

   return sourceVector 

end


function advect_z(f_in::Array{Float64,2},Uz::Vector,Rz::Matrix,g_low::Vector,g_high::Vector,rhs::Array{Float64,2},Minv::Matrix,dt::Float64)
   global Rz_low, Rz_high
   n = length(f_in[:,1])
   g = zeros(size(f_in))
   dgdt = zeros(n,p.Nz)
   result = zeros(size(f_in))

   dz = p.Lz/(p.Nz)

#   println("f(1) = $(f_in[:,1])")
#   println("f(2) = $(f_in[:,2])")

   # Advection is solved in a transformed basis g = inv(Rz)*f, where Rz is the matrix of generalized eigenvectors of the transport operator Tz with respect to mass matrix M.
   for iz in 1:p.Nz
      g[:,iz] = Rz\f_in[:,iz]
   end

   g_zp = zeros(size(g))
   g_zm = zeros(size(g))

   # Which index to use depends on the sign of Uz, the eigenvalues of the transport operator Tz
   for i in 1:n
      if Uz[i] > 0.0
         g_zp[i,:] = Uz[i]*g[i,:]
         g_zm[i,2:end] = Uz[i]*g[i,1:end-1]
         # Apply lower boundary condition
         if p.bndy_conds_z[1][1:4] == "refl"
            g_zm[i,1] = g_low[i]
         else
            g_zm[i,1] = g_low[i]*Uz[i]
         end
      elseif Uz[i] < 0.0
         g_zp[i,1:end-1] = Uz[i]*g[i,2:end]
         # Apply upper boundary condition
         if p.bndy_conds_z[2][1:4] == "refl"
            g_zp[i,end] = g_high[i]
         else
            g_zp[i,end] = g_high[i]*Uz[i]
         end
         g_zm[i,:] = Uz[i]*g[i,:]
      else
         g_zm[i,:] .= 0.0
         g_zp[i,:] .= 0.0
         if p.bndy_conds_z[1][1:4] == "refl"
            g_zm[i,1] = g_low[i]
         else
            g_zm[i,end] = Uz[i]*g_low[i]
         end
         if p.bndy_conds_z[2][1:4] == "refl"
            g_zp[i,end] = g_high[i]
         else
            g_zp[i,end] = Uz[i]*g_high[i]
         end
      end

   end

#   println("Lower BC: ")
#   println(g_low)
#   println("g at :")
#   println(g[:,1])
#   println("g_:")
#   println(g[:,1])
#   println("First point:")
#   println(g_zm[:,1])
#   println("Upper BC:")
#   println(g_high)
#   println("Penultimate point:")
#   println(g_zp[:,end-1])
#   println("Last point:") println(g_zp[:,end])
#   sleep(1)

   
#   n0,u0,T0,E0,M0,r0,s0,dummy = calculate_moments(Rz*g_low,p)
#   n1,u1,T1,E1,M1,r1,s1,dummy = calculate_moments(Rz*g_high,p)
#   println("$T0 , $T1")
#   n1,u1,T2,E1,M1,r1,s2,dummy = calculate_moments(Rz*g_zm[:,2],p)
#   n1,u1,T3,E1,M1,r1,s3,dummy = calculate_moments(Rz*g_zp[:,1],p)
#   n1,u1,T4,E1,M1,r1,s4,dummy = calculate_moments(Rz*g_zm[:,end],p)
#   n1,u1,T5,E1,M1,r1,s5,dummy = calculate_moments(Rz*g_zp[:,end-1],p)
#   n1,u1,T6,E1,M1,r1,s6,dummy = calculate_moments(Rz*g_zp[:,end],p)
#   n1,u1,T7,E1,M1,r1,s7,dummy = calculate_moments(Rz*g_high,p)
#   println(" ")
#   println("$T0, $T1, $T2, $T3, $T4, $T5, $T6, $T7")
   
#   println(g)
   for iz in 1:p.Nz
      rhs_g = Rz\(Minv*rhs[:,iz])
      dgdt[:,iz] = rhs_g - (g_zp[:,iz] - g_zm[:,iz])/dz
   end


   # Transform back to f
   for iz in 1:p.Nz
      result[:,iz] = f_in[:,iz] + dt*Rz*dgdt[:,iz]
   end

   return result
end

function dfdt_0d(f_in::Array{Float64,2},Minv::Matrix,E::Matrix,C::Array{Float64,3},Clin::Matrix,L::Matrix,S::Array{Float64,2})
   n = length(f_in[:,1])
   ci = zeros(n,p.Nz)

   result = zeros(size(f_in))
   
   for iz in 1:p.Nz
      f = f_in[:,iz]

      for i in 1:n
         Clocal = C[:,:,i]
         ci[i,iz] = sum((Clocal*f).*f) + (Clin*f)[i] + S[i,iz] - (L*f)[i] - (E*f)[i] 
      end

      result[:,iz] = Minv*ci[:,iz]
   end

   return result
end



function advanceTimestep_1d(f_in::Array{Float64,2},dt::Float64,timeMethod::String,nNewtonIter::Int64,M::Matrix,Minv::Matrix,E::Matrix,C::Array{Float64,3},Clin::Array{Float64,3},L::Array{Float64,3},S::Array{Float64,2},Tz::Matrix,Rz::Matrix,Uz::Vector,g_z_low::Vector,g_z_high::Vector,first::Bool)

   (timeMethod == "euler") ||  error("Higher order timestepping not available for 1D problems yet")

   n = length(f_in[:,1,1])
   rhs = zeros(n,p.Nz)

   result = zeros(size(f_in))
   
   dz = p.Lz/(p.Nz-1)

   # Calculate RHS of kinetic equation (everything except df/dt and spatial advection
   # Recalculate RHS for every substep of f?
   for iz in 1:p.Nz
      f = f_in[:,iz]

      for i in 1:n
         if p.collModel[1] == "zero"
            rhs[i,iz] = (Clin[:,:,iz]*f)[i] + S[i,iz] - (L[:,:,iz]*f)[i] - (E*f)[i] 
         else
            Clocal = C[:,:,i]
            rhs[i,iz] = sum((Clocal*f).*f) + (Clin[:,:,iz]*f)[i] + S[i,iz] - (L[:,:,iz]*f)[i] - (E*f)[i] 
         end
      end
   end

   if (p.Nz > 1)
      if p.bndy_conds_z[1][1:4] == "refl"
         f_z_low = getBoundaryCoeffsFromPreviousFlux(f_in[:,1],p,1,first)
         g_z_low_use = g_z_low + Uz.*(Rz\f_z_low)
      end
      if p.bndy_conds_z[2][1:4] == "refl"
         f_z_high = getBoundaryCoeffsFromPreviousFlux(f_in[:,end],p,2,first)
         g_z_high_use = g_z_high + Uz.*(Rz\f_z_high)
      end

      fcoeffs = advect_z(f_in,Uz,Rz,g_z_low_use,g_z_high_use,rhs,Minv,p.dt)
   else
      error("Using the wrong routine to advance timestep for this dimensionality")
   end

   return fcoeffs
end


function advanceTimestep_0d(f_in::Array{Float64,2},dt::Float64,timeMethod::String,nNewtonIter::Int64,M::Matrix,Minv::Matrix,E::Matrix,C::Array{Float64,3},Clin::Matrix,L::Matrix,S::Array{Float64,2})
   n = length(f_in[:,1,1])

   fcoeffs = deepcopy(f_in)

   if timeMethod == "euler"
      fcoeffs = fcoeffs + dt*dfdt_0d(fcoeffs,Minv,E,C,Clin,L,S)
   elseif timeMethod == "rk2"
      k1 = dt*dfdt_0d(fcoeffs,Minv,C,Clin,L,S)
      fcoeffs = fcoeffs + dt*dfdt_0d(fcoeffs+0.5*k1,Minv,E,C,Clin,L,S)
   elseif timeMethod == "rk4"
      k1 = dt*dfdt_0d(fcoeffs,Minv,E,C,Clin,L,S)
      k2 = dt*dfdt_0d(fcoeffs+0.5*k1,Minv,E,C,Clin,L,S)
      k3 = dt*dfdt_0d(fcoeffs+0.5*k2,Minv,E,C,Clin,L,S)
      k4 = dt*dfdt_0d(fcoeffs+k3,Minv,E,C,Clin,L,S)
      fcoeffs = fcoeffs + ((k1+k4)/6.0) + ((k2+k3)/3.0)
   elseif timeMethod == "backEuler"
      fcoeffs_iter = deepcopy(fcoeffs)
      for iter in 1:p.nNewtonIter

         res = zeros(n)
         D = zeros(n,n)
         for i in 1:n
            for j in 1:n
               for k in 1:n
                  res[i] = res[i] + C[j,k,i]*fcoeffs_iter[j]*fcoeffs_iter[k]
                  D[i,j] = D[i,j] + (C[j,k,i] + C[k,j,i])*fcoeffs_iter[k]
               end
               res[i] = res[i] + (Clin[i,j] - L[i,j] - E[i,j] )*fcoeffs_iter[j]
               D[i,j] = D[i,j] - (Clin[i,j] - L[i,j] - E[i,j] )
            end
            res[i] = res[i] + S[i] - (M*(fcoeffs_iter-fcoeffs))[i]/dt
         end
#         println("dim = $(countDim(D))")
         D = D - (1.0/dt)*M
         

         fcoeffs_iter = fcoeffs_iter - D\res
      end
      fcoeffs = deepcopy(fcoeffs_iter)
   else
      error("timeMethod = $timeMethod not found.")
   end

   return fcoeffs

end

function countDim(J::Matrix)
   s = LinearAlgebra.svdvals(J)
   smax = maximum(abs.(s))
   count = 0
   for i in 1:length(s)
      if abs(s[i])/smax < 1.0e-12
         count = count+1
      end
   end

   return count
      
end 

function invertWithPrecision(M::Array{Float64,2})
   M128 = zeros(Float128,size(M))
   M128[:,:] = M
   Minv = zeros(Float64,size(M))
   Minv[:,:] = inv(M128)
   return Minv
end


