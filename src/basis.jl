@everywhere vc = Float64[]
@everywhere vs = Float64[]
@everywhere vsnorm = Float64[]
@everywhere x_gm = Float64[]
@everywhere w_gm = Float64[]
@everywhere theta_leb = Float64[]
@everywhere phi_leb = Float64[]
@everywhere w_leb = Float64[]

function calculate_boundary_source(p::paramData)
   n = p.NLaguerre*p.NLegendre^2
   vref = sqrt(p.Tref/p.mass)
   Scoeffs = zeros(n,2)
   for ibound in 1:2
      sourceflux = p.bndy_z_sourceflux[ibound]
      vexp = p.bndy_z_vexp[ibound]
      vsign = (1.5-ibound)*2
      Tw = p.bndy_z_temp[ibound]
      vw = sqrt(Tw/p.mass)

      function sourcefunc(v::Vector)
         vmag=norm(v)
         if vsign*v[3] > 0.0
#            return ((vsign*v[3]/vref)^vexp)*exp(-0.5*vmag^2/vw^2)
            return ((vsign*v[3]/vmag)^vexp)*exp(-0.5*vmag^2/vw^2)
         else
            return 0.0
         end
      end

#      prefac = sourceflux*(vref/vw)^vexp / 
#      (vw^4*2*pi*(2^(0.5*(vexp-1)))*sf_gamma(0.5*(vexp+1)))

      prefac = sourceflux*((vexp+2.0)/(4.0*pi)) / vw^4

      Scoeffs[:,ibound] = prefac*getCoeffs(sourcefunc,p)
   end


   return Scoeffs[:,1], Scoeffs[:,2]
end

function calculate_boundary_f(p::paramData,Minv::Matrix)
   n = p.NLaguerre*p.NLegendre^2
   
   f_bound = zeros(n,2)

   for ibound in 1:2
      momvec = zeros(5)
      momvec[1] = p.bndy_z_dens[ibound]
      momvec[2:4] = [p.bndy_z_Vx[ibound], p.bndy_z_Vy[ibound], p.bndy_z_Vz[ibound]]
      momvec[5] = p.bndy_z_temp[ibound]

      if (p.bndy_conds_z[ibound] == "dirichlet") || (p.bndy_conds_z[ibound][1:4] == "refl")
         f_bound[:,ibound] = expand_fM_local(p,momvec[1],momvec[2:4],momvec[5])
         f_bound[:,ibound] = Minv*f_bound[:,ibound]
      elseif (p.bndy_conds_z[ibound] == "gradient")
         allmoms = zeros(n)
         allmoms[1:5] = momvec[1:5]
         f_bound[:,ibound] = Minv*allmoms
      elseif (p.bndy_conds_z[ibound] == "flux")
#         if sum(momvec[1]) >= 0.0
         if ibound == 1
            whichhalf = 1
         else
            whichhalf = -1
         end
         f_bound[:,ibound] = expand_fM_local_halfspace(p,momvec[1],momvec[2:4],momvec[5],whichhalf)
         p.bndy_conds_z[ibound] = "dirichlet"
      elseif (p.bndy_conds_z[ibound] == "mock_recycling")
         # This recycling option is a bit funny because 
         # the parameters are specified with the existing arrays
         # This will need to be changed
         if ibound == 1
            whichhalf = 1
         else
            whichhalf = -1
         end
         # Parameters needed for a bimaxwellian wall-source and their "encoding":
         # Vx: density ratio
         # Vy: temperature ratio of the "cold" part
         # Vz: Mean velocity of the "hot" part
#         f_bound[:,ibound] = expand_fM_local(p,momvec[1]*(1.0-momvec[2]),[0.0,0.0,momvec[4]],momvec[5])
#         f_bound[:,ibound] += expand_fM_local(p,momvec[1]*momvec[2],[0.0,0.0,0.0],momvec[5]*momvec[3])
         f_bound[:,ibound] = expand_fM_local_halfspace(p,momvec[1]*(1.0-momvec[2]),[0.0,0.0,momvec[4]],momvec[5],whichhalf)
         f_bound[:,ibound] += expand_fM_local_halfspace(p,momvec[1]*momvec[2],[0.0,0.0,0.0],momvec[5]*momvec[3],whichhalf)

#         f_bound[:,ibound] = Minv*f_bound[:,ibound]

         # Treat like Dirichlet from here on
         p.bndy_conds_z[ibound] = "dirichlet"
      else
         error("Boundary condition $(p.bndy_conds_z[ibound]) not recognized.")
      end
   end

   #n0,u0,T0,E0,M0,r0,s0,q0,dummy = calculate_moments(f_bound[:,1],p)
   #n1,u1,T1,E1,M1,r1,s1,q1,dummy = calculate_moments(f_bound[:,2],p)
   #println("T0 = $T0 , T1 = $T1")

   return f_bound[:,1], f_bound[:,2]
end

function calculate_v_arrays(p::paramData)
   global vc,vs,vsnorm,x_gm,w_gm,theta_leb,phi_leb,w_leb,x_gl,w_gl

   vref = sqrt(p.Tref/p.mass)

   x_gm, w_gm, x_gl, w_gl = get_v_quadrules(p)
   theta_leb, phi_leb, w_leb =  genlebedevrules(p.NSphere)

   vs = zeros(3,p.NGauss,p.NSphere)
   vsnorm = zeros(3,p.NGauss,p.NSphere)
   vc = zeros(3,p.NGauss,p.NSphere)

   for iv in 1:p.NGauss
      for jv in 1:p.NSphere
         vs[:,iv,jv] .= [sqrt(x_gm[iv]),theta_leb[jv],phi_leb[jv]]
         vsnorm[:,iv,jv] .= [sqrt(x_gm[iv])/vref,theta_leb[jv],phi_leb[jv]]
         vc[:,iv,jv] .= spherical2cartesian(vs[:,iv,jv])
      end
   end

end 

# Calculates the half-space integrals of boundary distribution
function expand_fM_local_halfspace(p::paramData,dens::Float64,flow::Vector,temp::Float64,whichhalf::Integer)

   vref = sqrt(p.Tref/p.mass)

   n = p.NLaguerre*p.NLegendre^2
   NGL = p.NGauss
   NL = p.NSphere
   Lmax = p.NLegendre-1

   function finit(vs::Vector)
      sum = 0.0
      vc = spherical2cartesian(vs)
      for i in 1:length(dens)
         sum += dens[i]*fM((vc-flow[:,i]),temp[i]/p.mass)
      end
      return sum
   end

   Mhalf = zeros(n,n)
   fhalf = zeros(n)

   @info "Building halfspace matrix..."
   for i in 1:n
      ki,li,mi = get_idx(i,Lmax)
      for j in 1:n
         kj,lj,mj = get_idx(j,Lmax)
         
         function Mintegrand(vnorm)
            return basis_function(vnorm,ki,li,mi) * basis_function(vnorm,kj,lj,mj)
         end
         Mhalf[i,j] = integrate_half_vspace(Mintegrand,whichhalf)
      end
      function fintegrand(vnorm)
 
         return basis_function(vnorm,ki,li,mi) * finit(vnorm.*[vref,1.0,1.0]) * vref^3/sum(p.initDens)
      end

      fhalf[i] = integrate_half_vspace(fintegrand,whichhalf) * sum(p.initDens)/vref^3
   end
   @info " done."


   return Mhalf\fhalf
end

function expand_fM_local(p::paramData,dens::Float64,flow::Vector,temp::Float64)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb, vc

   vref = sqrt(p.Tref/p.mass)

   n = p.NLaguerre*p.NLegendre^2
   NGL = p.NGauss
   NL = p.NSphere
   Lmax = p.NLegendre-1

   f0 = zeros(n)
   for i in 1:n
      k,l,m = get_idx(i,Lmax)

      for iv in 1:NGL
         tot = 0.0
         for jv in 1:NL

            tot += w_leb[jv] * fM(vc[:,iv,jv]-flow,temp/p.mass) * test_function(vsnorm[:,iv,jv],k,l,m)
#            tot += w_leb[jv] * fM(vc[:,iv,jv]-flow,temp/p.mass) * basis_function(vsnorm[:,iv,jv],k,l,m) / vref^3
         end
         f0[i] += dens*w_gm[iv]*exp(0.5*x_gm[iv]/vref^2) * tot
      end
   end

   return f0
end

function initialize_sumMaxw(Minv::Matrix,p::paramData)
   if p.Nz > 1
      error("Ambigous initial condition. When Nz>1, initialize_sumMaxw should not have been used.")
   end

   n = p.NLaguerre*p.NLegendre^2

   f0 = zeros(n)
   for i in 1:length(initDens)
      u = [p.initVx[i],p.initVy[i],p.initVz[i]]
      f0 += expand_fM_local(p,p.initDens[i],u,p.initTemp[i])
   end
   return Minv*f0
   return f0
end

function get_1d_params(p::paramData)

   dens = zeros(p.Nz)
   flow = zeros(3,p.Nz)
   temp = zeros(p.Nz)

   dz = p.Lz/(p.Nz)
   if p.Nz > 1
      z_grid = collect(range(0.5*dz,p.Lz-0.5*dz,length=p.Nz))
   else
      z_grid = [0.0]
   end

   if ( (p.bndy_conds_z[1] == "dirichlet") || (p.bndy_conds_z[1][1:4] == "refl")) && ( (p.bndy_conds_z[2] == "dirichlet") || (p.bndy_conds_z[2][1:4] == "refl"))

      n_slope = (p.bndy_z_dens[2] - p.bndy_z_dens[1]) / (p.Lz+2*dz)
      Vx_slope = (p.bndy_z_Vx[2] - p.bndy_z_Vx[1]) / (p.Lz+2*dz)
      Vy_slope = (p.bndy_z_Vy[2] - p.bndy_z_Vy[1]) / (p.Lz+2*dz)
      Vz_slope = (p.bndy_z_Vz[2] - p.bndy_z_Vz[1]) / (p.Lz+2*dz)
      T_slope = (p.bndy_z_temp[2] - p.bndy_z_temp[1]) / (p.Lz+2*dz)

      dens = p.bndy_z_dens[1] .+ n_slope * (z_grid)
      flow[1,:] = p.bndy_z_Vx[1] .+ Vx_slope * (z_grid)
      flow[2,:] = p.bndy_z_Vy[1] .+ Vy_slope * (z_grid)
      flow[3,:] = p.bndy_z_Vz[1] .+ Vz_slope * (z_grid)
      temp = p.bndy_z_temp[1] .+ T_slope * (z_grid)
   elseif (p.bndy_conds_z[1] == "dirichlet") || (p.bndy_conds_z[2] == "dirichlet")
      if p.bndy_conds_z[1] == "dirichlet"
         dside = 1
      else
         dside = 2
      end

      dens .= p.bndy_z_dens[dside]
      flow[1,:] .= p.bndy_z_Vx[dside]
      flow[2,:] .= p.bndy_z_Vy[dside]
      flow[3,:] .= p.bndy_z_Vz[dside]
      temp .= p.bndy_z_temp[dside]
   else
      error("Ambiguous boundary condition specification. No Dirichlet condition means that get_1d_params should not be called.")
   end
   return dens,flow,temp
end

function initialize(Minv::Matrix,p::paramData)

   fcoeffs = zeros(Float64,(p.NLaguerre*p.NLegendre^2,p.Nz))

   ### Initialize according to BCs
   # If distribution is not set at either boundary, use "initDens", etc. just like the 0D case.
#   if (p.Nz == 1) || (p.bndy_conds_z[1] != "dirichlet") && (p.bndy_conds_z[2] != "dirichlet")
   if (p.Nz == 1) #|| (p.bndy_conds_z[1] != "dirichlet") && (p.bndy_conds_z[2] != "dirichlet")
      for i in 1:length(p.initDens)
         u = [p.initVx[i],p.initVy[i],p.initVz[i]]

         fcoeffs[:,1] += expand_fM_local(p,p.initDens[i],u,p.initTemp[i])
      end
      for iz in 2:p.Nz
         fcoeffs[:,iz] .= fcoeffs[:,1]
      end
   # Otherwise, set to a linear combination between the two sides
   else
      dens,flow,temp = get_1d_params(p)
      for iz in 1:p.Nz
         fcoeffs[:,iz] = expand_fM_local(p,dens[iz],flow[:,iz],temp[iz])
      end
   end

   for iz in 1:p.Nz
      fcoeffs[:,iz] = Minv*fcoeffs[:,iz]
   end

   return fcoeffs

end 

function get_v_quadrules(p::paramData)

   vref = sqrt(p.Tref/p.mass)
   
   # Function over which the Gaussian quadrature is orthogonal with respect to. 
   # Needs a single argument
   function g_GaussMaxwell(x::Float128)
#      return sqrt(x)*exp(-0.5*x) 
      return 0.5*sqrt(x)*exp(-0.5*x) 
   end

   function g_GaussLaguerre(v::Float128)
      return exp(-0.5*v^2) 
   end

   x_gm, w_gm = Genquad.genquadrules(g_GaussMaxwell,p.NGauss,[Float128(0.0),Float128(Inf)])

   x_gl, w_gl = Genquad.genquadrules(g_GaussLaguerre,p.NGauss,[Float128(0.0),Float128(Inf)])

#   x_gl, w_gl = gausslaguerre(p.NGauss)

   x_gm = x_gm * vref^2
   w_gm = w_gm * vref^3

   x_gl = x_gl * vref^2
   w_gl = w_gl * vref^3

   return x_gm, w_gm, x_gl, w_gl
end

@everywhere function basisnormfac(k::Int64,l::Int64)
   return sqrt( 2*factorial(float(k)) / sf_gamma(k+l+1.5))
end

# Obtains Lebedev quadrature rules for the unit sphere from the downloaded text files
function genlebedevrules(NL)

   filename = "$(@__DIR__)/../data/lebedevrules/lebedev_$(lpad(NL,4,"0")).txt"

   data = try
      readdlm(filename)
   catch
      error("Could not find Lebedev quadrature file $filename")
   end

   phi = data[:,1] * pi/180.0
   theta = data[:,2] * pi/180.0
   w = data[:,3]

   if length(phi) != NL
      error("Length of data in file does not match NL=$NL")
   end

   e_lebedev = [theta phi]
   w = w*4.0*pi
   
   return theta, phi, w
end


# Implementation of spherical harmonics consistent with Gamba normalization
@everywhere function Ylm(l::Int64,m::Int64,theta::Float64,phi::Float64)
   x = cos(theta)

   Plm = sf_legendre_Plm(l,abs(m),x)
   # Library returns P_l^m. Gamba uses P_lm.
   Plm = ((-1.0)^m)*Plm

   # Normalization factor
   Ynormfac = sqrt( ((2*l+1) * factorial(l-abs(m)) ) / ( 2*pi*factorial(l+abs(m)) ) )
   if m == 0
      result = Ynormfac*sqrt(0.5) * Plm
   elseif m > 0
      result = Ynormfac*Plm*cos(m*phi)
   elseif m < 0
      result = Ynormfac*Plm*sin(abs(m)*phi)
   else
      error("Something went horribly wrong")
   end
     
   return result
end

# psi_i from the paper
@everywhere function test_function(v::Vector,k::Int64,l::Int64,m::Int64)

   # Spherical coords:
   vmag = v[1]
   theta = v[2]
   phi = v[3]

   psi = basisnormfac(k,l) * sf_laguerre_n(k,l+0.5,vmag^2) * (vmag^l) * Ylm(l,m,theta,phi)

   return psi
end

# phi_j from the paper
@everywhere function basis_function(v::Vector,k::Int64,l::Int64,m::Int64)
   vmag = v[1]

   phi =  exp(-0.5*vmag^2) * test_function(v,k,l,m) 

   return phi
end

# Returns f(v) given basis coefficients, with v a cartesian 3-vector
function get_f(vc::Vector,fcoeffs_in::Vector,p::paramData)

   fcoeffs = fcoeffs_in
   n = length(fcoeffs)
   Lmax = p.NLegendre-1
   vref = sqrt(p.Tref/p.mass)

   vs = zeros(3)

   tot = 0.0
   for i in 1:n
      k,l,m = get_idx(i,Lmax)
      vs = cartesian2spherical(vc)
      vsnorm = vs./[vref,1.0,1.0]

      tot += fcoeffs[i]*basis_function(vsnorm,k,l,m)
   end

   return tot
end

function exactMomentsVSt(n0::Float64,u0::Vector,T0::Float64,M0::Matrix,r0::Vector,s0::Float64,t::Float64)

   M = M0*exp(-0.5*t) + (T0*one(M0) + u0*transpose(u0))*(1.0-exp(-0.5*t))
   r = r0*exp(-t/3.0) + (5.0*T0 + norm(u0)^2)*u0*(1.0-exp(-t/3.0)) + 
       2.0*(M0-u0*transpose(u0) - T0*one(M0))*u0*(exp(-t/2.0)-exp(-t/3.0))

   s = s0*exp(-t/3.0) + (norm(u0)^4 + 15*T0^2 + 10*T0*norm(u0)^2)*(1.0-exp(-t/3.0)) + 
      0.5*(norm(M0)^2 - 3.0*T0^2 + norm(u0)^4 - 2.0*dot(M0*u0,u0))*(exp(-t)-exp(-t/3.0)) + 
      4.0*(dot(M0*u0,u0) - norm(u0)^4 - T0*norm(u0)^2)*(exp(-t/2.0)-exp(-t/3.0))


   return M[1,1], r[1], s

end

function getSphericalCoeffs(f::Function,p::paramData)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1
   vref = sqrt(p.Tref/p.mass)

   f0 = zeros(n)
   for i in 1:n
      k,l,m = get_idx(i,Lmax)

      for iv in 1:p.NGauss
         tot = 0.0
         for jv in 1:p.NSphere
            vs = [sqrt(x_gm[iv]),theta_leb[jv],phi_leb[jv]]
            vsnorm = [sqrt(x_gm[iv])/vref,theta_leb[jv],phi_leb[jv]]

            tot += w_leb[jv] * f(vs) * basis_function(vsnorm,k,l,m)
         end
         f0[i] += 2.0*pi*w_gm[iv]*exp(0.5*x_gm[iv]/vref^2) * tot/vref^3
      end
   end

   return f0
end


function getCoeffs(f::Function,p::paramData)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1
   vref= sqrt(p.Tref/p.mass)

   f0 = zeros(n)
   for i in 1:n
      k,l,m = get_idx(i,Lmax)


      for iv in 1:p.NGauss
         tot = 0.0
         for jv in 1:p.NSphere
            vs = [sqrt(x_gm[iv]),theta_leb[jv],phi_leb[jv]]
            vsnorm = [sqrt(x_gm[iv])/vref,theta_leb[jv],phi_leb[jv]]

            v = spherical2cartesian(vs)

            tot += w_leb[jv] * f(v) * test_function(vsnorm,k,l,m)
#            tot += w_leb[jv] * f(v) * basis_function(vsnorm,k,l,m)/vref^3
         end
         f0[i] += w_gm[iv]*exp(0.5*x_gm[iv]/vref^2) * tot
      end
   end

   return f0
end

function integrate_half_vspace(f::Function,whichhalf::Integer)
   if whichhalf == 1
      theta_lower = 0.0
      theta_upper = 0.5*pi
   elseif whichhalf == -1
      theta_lower = 0.5*pi
      theta_upper = pi
   else
      error("Value of whichhalf in integrate_halfvspace not valid.")
   end

   res,err = hcubature( y-> (y[1]/(1.0-y[1]))^2 * (1.0/(1-y[1])^2)*sin(y[2])*f([ y[1]/(1.0-y[1]), y[2],y[3] ]),[0.0,theta_lower,0.0],[1.0,theta_upper,2.0*pi],abstol=1.0e-9,reltol=1.0e-9)
   return res
end



# This and integratevspace aren't expected to work if v is not normalized
# Removing it for now since it's not used anywhere.
#function getExactCoeffs(f::Function,NLaguerre::Int64,NLegendre::Int64)
#
#   n = NLaguerre*NLegendre^2
#   Lmax = NLegendre-1
#
#   f0 = zeros(n)
#   for i in 1:n
#      k,l,m = get_idx(i,Lmax)
#
#      function integrand(v::Vector)
#         return f(v)*basis_function(v,k,l,m)
#      end
#
#      f0[i] = integratevspace(integrand)
#   end
#
#   return f0
#end
#
function integratevspace(f::Function)
   res,err = hcubature( y-> (y[1]/(1.0-y[1]))^2 * (1.0/(1-y[1])^2)*sin(y[2])*f([ y[1]/(1.0-y[1]), y[2],y[3] ]),[0.0,0.0,0.0],[1.0,pi,2.0*pi],abstol=1.0e-9)
   return res
end

# Adapted from Torsten Kessler's notes and implementation (private correspondence, 2019)
# Accepts a pairs of spherical harmonic indices and returns three arrays:
#   l_array = array of l values of the single S.H. series
#   m_array = array of m values of the single S.H. series
#   c = array of coefficients that multiply the above S.H.s
# These are constructed so that:
#   Y(l1,m1)*Y(l2,m2) = sum(i){ c_array[i]*Y(l_array[i],m_array[i]) }
function harmonic_prod(l1_in::Int64,m1_in::Int64,l2_in::Int64,m2_in::Int64)

   l1_in >= 0 || error("l1_in negative")
   l2_in >= 0 || error("l2_in negative")
   abs(m1_in) <= l1_in || error("Invalid value of m1_in")
   abs(m2_in) <= l2_in || error("Invalid value of m1_in")

   l_array = Int64[]
   m_array = Int64[]
   c_array = Float64[]

   # Make sure m1 > m2
   if m2_in > m1_in
      m1 = m2_in
      m2 = m1_in
      l1 = l2_in
      l2 = l1_in
   else
      m1 = m1_in
      m2 = m2_in
      l1 = l1_in
      l2 = l2_in
   end

   n(m) = (m==0) ? 1 : sqrt(2)

   function sg(m1_arg::Int64,m2_arg::Int64)
      if m1_arg > abs(m2_arg)
         return 1
      elseif m1_arg == abs(m2_arg)
         return 0
      else
         return (-1)^(m1_arg + m2_arg+1)
      end
   end

   function sn(m_arg::Int64)
      return (-1)^(m_arg)
   end

   function wigner3j(L1,L2,L,M1,M2)
      M=-M1-M2
      return sf_coupling_3j(2*L1,2*L2,2*L,2*M1,2*M2,2*M)
   end

   for l in abs(l1-l2):(l1+l2)

      prefac = 0.5*sn(m1+m2)*n(m1)*n(m2)*sqrt(2*l+1)*wigner3j(l1,l2,l,0,0)*sqrt( (2*l1+1)*(2*l2+1)/(4.0*pi) )

      if m1 >= 0 && m2 >= 0

         if abs(m1+m2) <= l
            append!(l_array,l)
            append!(m_array, m1+m2)
            append!(c_array,prefac*(1.0/n(m1+m2))*wigner3j(l1,l2,l,m1,m2))
         end

         if abs(m1-m2) <= l
            append!(l_array, l)
            append!(m_array, m1-m2)
            append!(c_array,sn(m2)*prefac*(1.0/n(m1-m2))*wigner3j(l1,l2,l,m1,-m2))
         end

      elseif  m1 >= 0 && m2 < 0

         if abs(m1+abs(m2)) <= l
            append!(l_array, l)
            append!(m_array, -(m1+abs(m2)) )
            append!(c_array,
              prefac*(1.0/n(m1+abs(m2)))*wigner3j(l1,l2,l,m1,abs(m2))
            )
         end

         if abs(m1-abs(m2)) <= l
            append!(l_array, l)
            append!(m_array, -abs(m1-abs(m2)))
            append!(c_array,
              -prefac*(sn(m2)*sg(m1,m2)/n(m1-abs(m2)))*wigner3j(l1,l2,l,m1,-abs(m2))
            )
         end

      else

         if abs(abs(m1) + abs(m2)) <= l
            append!(l_array, l)
            append!(m_array, abs(m1) + abs(m2))
            append!(c_array,
              -prefac*(1.0/n(abs(m1)+abs(m2)))*wigner3j(l1,l2,l,abs(m1),abs(m2))
            )
         end

         if abs(abs(m2) - abs(m1)) <= l
            append!(l_array, l)
            append!(m_array, abs(m2) - abs(m1))
            append!(c_array,
                    prefac*(sn(m1)/n(abs(m1)-abs(m2)))*wigner3j(l1,l2,l,abs(m1),-abs(m2))
            )
         end

      end

   end

   return l_array, m_array, c_array
end

function integrate_SHprod_halfspace(l1::Int64,m1::Int64,l2::Int64,m2::Int64,g,whichhalf::Int64)

   l_array,m_array,coeff_array = harmonic_prod(l1,m1,l2,m2)

   sum = 0.0
   for i in 1:length(l_array)
      if typeof(g) == Array{Float64,1}
         g_l = g[l_array[i]+1]
      else
         function integrand_gl(x::Vector)
            return g(x[1])*sf_legendre_Pl(l_array[i],cos(x[1]))*sin(x[1])
         end
         g_l,err = hcubature(integrand_gl,[0.0],[0.5*pi],reltol=1.0e-12,abstol=1.0e-14)
      end

      if whichhalf == 1
         prefac = Ylm(l_array[i],m_array[i],0.0,0.0)
      elseif whichhalf == -1
         prefac = Ylm(l_array[i],m_array[i],1.0*pi,0.0)
      else
         error("Invalid value of whichhalf passed to integrate_SHprod_halfspace")
      end
      sum += 2.0*pi*prefac*g_l*coeff_array[i]
   end
   return sum
end

function test_halfspace_integrals()
   # Integrate a product of spherical harmonics (times g(theta)) over both halfspaces (z>0 and z<0)
   # Compare to numerical result
   
   l1_array = [0,1,1,2,3,4]
   m1_array = [0,0,-1,0,2,-1]

   l2_array = [1,1,2,2,3,4]
   m2_array = [0,0,-1,2,-3,0]

   @testset "Integrating products of spherical harmonics over z>0 halfspace." begin
      function g(theta::Float64)
         return sin(theta)^2*cos(theta)^2
      end

      for i in 1:length(l1_array)
         l1 = l1_array[i]
         l2 = l2_array[i]
         m1 = m1_array[i]
         m2 = m2_array[i]

         function test_integrand(v::Vector)
            theta=v[1]
            phi=v[2]
            return Ylm(l1,m1,theta,phi)*Ylm(l2,m2,theta,phi)*sin(theta)*g(theta)
         end

         numerical,err = hcubature(test_integrand,[0.0,0.0],[0.5*pi,2.0*pi],abstol=1.0e-12,reltol=1.0e-12)
         result = integrate_SHprod_halfspace(l1,m1,l2,m2,g,1)

#         println("$(l1_array[i]), $(m1_array[i]) x $(l2_array[i]), $(m2_array[i])")
         @test numerical ≈ result atol=1.0e-5

      end
   end;
   @testset "Integrating products of spherical harmonics over z<0 halfspace." begin
      function g(theta::Float64)
         return sin(theta)^2*cos(theta)^2
      end

      for i in 1:length(l1_array)
         l1 = l1_array[i]
         l2 = l2_array[i]
         m1 = m1_array[i]
         m2 = m2_array[i]

         function test_integrand(v::Vector)
            theta=v[1]
            phi=v[2]
            return Ylm(l1,m1,theta,phi)*Ylm(l2,m2,theta,phi)*sin(theta)*g(theta)
         end

         numerical,err = hcubature(test_integrand,[0.5*pi,0.0],[1.0*pi,2.0*pi],abstol=1.0e-12,reltol=1.0e-12)
         result = integrate_SHprod_halfspace(l1,m1,l2,m2,g,-1)

         @test numerical ≈ result atol=1.0e-5
      end
   end;
end

function test_single_harmonic_prod(l1::Int64,m1::Int64,l2::Int64,m2::Int64,theta::Float64,phi::Float64)
   test1 = Ylm(l1,m1,theta,phi)*Ylm(l2,m2,theta,phi)

   l,m,c = harmonic_prod(l1,m1,l2,m2)
#   println(l)
#   println(m)
#   println(c)
   sum = 0.0
   for i in 1:length(c)
      if abs(m[i]) <= l[i]
         sum+= c[i] * Ylm(l[i],m[i],theta,phi)
      end
   end
   test2 = sum

#   println("$test1, $test2")
   return test1,test2
end

function test_harmonic_prods(theta=0.8,phi=0.2)
   l_test = [0,1,2,3]
   @testset "Expansion of product of two real spherical harmonics." begin
   for l1 in l_test
      for l2 in l_test
         for m1 in -l1:l1
            for m2 in -l2:l2
               exact,result = test_single_harmonic_prod(l1,m1,l2,m2,theta,phi)
               @test exact ≈ result atol=1.0e-12
            end
         end
      end
   end
   end;
end



