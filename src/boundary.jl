function buildBoundaryMatrices(p::paramData)
   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1
   #
   # _plus and _minus appendages indicate integration over halfspaces z>0 and z<0, respectively
   # Which to use depends on if the left or right boundary is reflecting
   M_left = zeros(n,n)
   M_right = zeros(n,n)
   b_left = zeros(n)
   b_right = zeros(n)
   om_left = zeros(n)
   om_right = zeros(n)


   # Maximum order of polynomial (in v^2 to integrate)
   maxOrder_v = (
        4*(p.NLaguerre-1)   # Laguerre polynomials
      + 2*(p.NLegendre-1)   # Powers of v^l
      + 3)                  # Integration measure (power of 2)
                            # + another power of 1 for rounding
   maxOrder_xi = 2*(p.NLegendre-1)

   # For exact polynomial integration:
   # Nint = 0.5*maxOrder_v + 0.5
   # Add one for rounding
   Nint_v = round(Int64,0.5*maxOrder_v+1.5)
   Nint_xi = round(Int64,0.5*maxOrder_xi+1.5)

   ############################
   # Prepare quadrature rules
   
   # Integrals in v
   # Not to be confused with the global variables x_gl and w_gl
   # (need to fix this confusion)
   vgm,wvgm = Genquad.genquadrules(x->x^2*exp(-x^2),Nint_v,[Float128(0.0),Float128(Inf)])
   vgm_h,wvgm_h = Genquad.genquadrules(x->x^2*exp(-0.5*x^2),Nint_v,[Float128(0.0),Float128(Inf)])

   Tleft_fac = 0.5*( 1.0 + (p.Tref/p.bndy_z_temp[1]) )
   Tright_fac = 0.5*( 1.0 + (p.Tref/p.bndy_z_temp[2]) )
   vgm_left,wvgm_left = Genquad.genquadrules(x->x^2*exp(-Tleft_fac*x^2),Nint_v,[Float128(0.0),Float128(Inf)])
   vgm_right,wvgm_right = Genquad.genquadrules(x->x^2*exp(-Tright_fac*x^2),Nint_v,[Float128(0.0),Float128(Inf)])

   # Luckily, the integrals over phi are trivial. Only need Legendre polynomials in cos(theta)
#   x_t,w_t = Genquad.genquadrules(x->exp(-x),Nint_v,[Float128(0.0),Float128(Inf)])
   x_xi,w_xi = gausslegendre(Nint_xi)

   # In order to do the half-space integrals exactly, we need to separate the energy and angular parts.
   # Also, need to be selective in what quadrature scheme to use, depending on the order of polynomials 
   # in v, and what Gaussian factors there are.
   # 
   # Goal is the find the eigenvectors of a particular matrix corresponding to eigenvalue zero.
   # Since this vector is normalized, we will have to find the overall density by some other means.
   # This is either input as p.bndy_z_dens or set by incoming the ion flux (for recycling boundary conditions).
   
   # Need to calculate arrays representing integrals over Legendre polynomials
   # g_l = int[0,1](P_l(x))
   # g_l_x = int[0,1](P_l(x)*x)
   g_l_x = zeros(maxOrder_xi+1)
   g_l = zeros(maxOrder_xi+1)


   x_leg_half,w_leg_half = Genquad.genquadrules(x->1.0,Nint_xi,[Float128(0.0),Float128(1.0)])
   for il in 1:maxOrder_xi+1
      for ixi in 1:Nint_xi
         g_l[il] += w_leg_half[ixi]*sf_legendre_Pl(il-1,x_leg_half[ixi])
         g_l_x[il] += w_leg_half[ixi]*x_leg_half[ixi]*sf_legendre_Pl(il-1,x_leg_half[ixi])
      end
   end
   
   for i in 1:n
      ki, li, mi = get_idx(i,Lmax)
      for j in 1:n
         kj, lj, mj = get_idx(j,Lmax)

         energyIntegral = 0.0

         polyOrder_v = 2*ki + 2*kj + li + lj + 1
         polyOrder_v <= 2.0*maxOrder_v || error("Polynomial in v exceeds maxOrder_v for exact integration.\n polyOrder_v = $polyOrder_v, maxOrder_v = $maxOrder_v")

         for iv in 1:Nint_v
            energyIntegral += wvgm[iv]*(vgm[iv]^(li+lj))*sf_laguerre_n(ki,li+0.5,vgm[iv]^2)*sf_laguerre_n(kj,lj+0.5,vgm[iv]^2)
         end

         angularIntegral_left = integrate_SHprod_halfspace(li,mi,lj,mj,g_l,1)
         angularIntegral_right = integrate_SHprod_halfspace(li,mi,lj,mj,g_l,-1)
#         angularIntegral_left = integrate_SHprod_halfspace(li,mi,lj,mj,x->1.0,1)
#         angularIntegral_right = integrate_SHprod_halfspace(li,mi,lj,mj,x->1.0,-1)

         M_left[i,j] = energyIntegral*angularIntegral_left * basisnormfac(ki,li) * basisnormfac(kj,lj)
         M_right[i,j] = energyIntegral*angularIntegral_right * basisnormfac(ki,li) * basisnormfac(kj,lj)
      end

      energyIntegral = 0.0
      for iv in 1:Nint_v
         energyIntegral += wvgm_h[iv]*(vgm_h[iv]^(li+1))*sf_laguerre_n(ki,li+0.5,vgm_h[iv]^2)
      end

      angularIntegral_left = integrate_SHprod_halfspace(li,mi,1,0,g_l,-1)*sqrt(4.0*pi/3.0)
      angularIntegral_right = integrate_SHprod_halfspace(li,mi,1,0,g_l,1)*sqrt(4.0*pi/3.0)
#      angularIntegral_left = integrate_SHprod_halfspace(li,mi,1,0,x->1.0,-1)*sqrt(4.0*pi/3.0)
#      angularIntegral_right = integrate_SHprod_halfspace(li,mi,1,0,x->1.0,1)*sqrt(4.0*pi/3.0)

      om_left[i] = -energyIntegral*angularIntegral_left*basisnormfac(ki,li)
      om_right[i] = energyIntegral*angularIntegral_right*basisnormfac(ki,li)

      energyIntegral_left = 0.0
      energyIntegral_right = 0.0
      for iv in 1:Nint_v
         energyIntegral_left += wvgm_left[iv]*vgm_left[iv]^(li)*sf_laguerre_n(ki,li+0.5,vgm_left[iv]^2)
         energyIntegral_right += wvgm_right[iv]*vgm_right[iv]^(li)*sf_laguerre_n(ki,li+0.5,vgm_right[iv]^2)
      end
      angularIntegral_left = integrate_SHprod_halfspace(li,mi,0,0,g_l,1)*sqrt(4.0*pi)  
      angularIntegral_right = integrate_SHprod_halfspace(li,mi,0,0,g_l,-1)*sqrt(4.0*pi) 
#      angularIntegral_left = integrate_SHprod_halfspace(li,mi,0,0,x->1.0,1)*sqrt(4.0*pi)  
      #angularIntegral_right = integrate_SHprod_halfspace(li,mi,0,0,x->1.0,-1)*sqrt(4.0*pi) 

      b_left[i] = energyIntegral_left*angularIntegral_left*basisnormfac(ki,li)/(2.0*pi*(p.bndy_z_temp[1]/p.mass)^2)
      b_right[i] = energyIntegral_right*angularIntegral_right*basisnormfac(ki,li)/(2.0*pi*(p.bndy_z_temp[2]/p.mass)^2)

      vref = sqrt(p.Tref/p.mass)

      function left_integrand(v::Vector)
#         return basis_function([v[1]*sqrt(p.Tref/p.bndy_z_temp[1]),v[2],v[3]],ki,li,mi)*exp(-0.5*(p.Tref/p.bndy_z_temp[1])*( (v[1]*sin(v[2])*cos(v[3])-p.bndy_z_Vx[1]/vref)^2 + (v[1]*sin(v[2])*sin(v[3])-p.bndy_z_Vy[1]/vref)^2 + (v[1]*cos(v[2])-p.bndy_z_Vz[1]/vref)^2) )
#         return basis_function(v,ki,li,mi)*exp(-0.5*(p.Tref/p.bndy_z_temp[1])*( (v[1]*sin(v[2])*cos(v[3])-(p.bndy_z_Vx[1]/vref))^2 + (v[1]*sin(v[2])*sin(v[3])-(p.bndy_z_Vy[1]/vref))^2 + (v[1]*cos(v[2])-(p.bndy_z_Vz[1]/vref))^2) )*v[1]*cos(v[2])
         return basis_function(v,ki,li,mi)*exp(-0.5*(p.Tref/p.bndy_z_temp[1])*( (v[1]*sin(v[2])*cos(v[3])-(p.bndy_z_Vx[1]/vref))^2 + (v[1]*sin(v[2])*sin(v[3])-(p.bndy_z_Vy[1]/vref))^2 + (v[1]*cos(v[2])-(p.bndy_z_Vz[1]/vref))^2) )
      end

      function right_integrand(v::Vector)
#         return basis_function([v[1]*sqrt(p.Tref/p.bndy_z_temp[2]),v[2],v[3]],ki,li,mi)*exp(-0.5*(p.Tref/p.bndy_z_temp[2])*( (v[1]*sin(v[2])*cos(v[3])-p.bndy_z_Vx[2]/vref)^2 + (v[1]*sin(v[2])*sin(v[3])-p.bndy_z_Vy[2]/vref)^2 + (v[1]*cos(v[2])-p.bndy_z_Vz[2]/vref)^2) )
#return -basis_function(v,ki,li,mi)*exp(-0.5*(p.Tref/p.bndy_z_temp[2])*( (v[1]*sin(v[2])*cos(v[3])-p.bndy_z_Vx[2]/vref)^2 + (v[1]*sin(v[2])*sin(v[3])-p.bndy_z_Vy[2]/vref)^2 + (v[1]*cos(v[2])-p.bndy_z_Vz[2]/vref)^2))*v[1]*cos(v[2]) 
         return basis_function(v,ki,li,mi)*exp(-0.5*(p.Tref/p.bndy_z_temp[2])*( (v[1]*sin(v[2])*cos(v[3])-p.bndy_z_Vx[2]/vref)^2 + (v[1]*sin(v[2])*sin(v[3])-p.bndy_z_Vy[2]/vref)^2 + (v[1]*cos(v[2])-p.bndy_z_Vz[2]/vref)^2))
      end
      b_left[i] =  integrate_half_vspace(left_integrand,1)* (p.mass/(p.bndy_z_temp[1]))^2.0 / (2.0*pi)
      b_right[i] =  integrate_half_vspace(right_integrand,-1)*(p.mass/(p.bndy_z_temp[2]))^2.0 / (2.0*pi)
   end

   return M_left, M_right, b_left, b_right, om_left, om_right

end

function specularReflectionCoeffs(f_prev::Vector,p::paramData,first::Bool)
   global specrefl_fac

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1

   if first
      specrefl_fac = ones(n)
      for i in 1:n
         ki,li,mi = get_idx(i,Lmax)
         specrefl_fac[i] = (-1)^(li+mi)
      end
   end

   return specrefl_fac.*f_prev
end

function getBoundaryCoeffsFromPreviousFlux(f_prev::Vector,p::paramData,zidx::Int64,first::Bool)
   global M_left, M_right, b_left, b_right, om_left, om_right, Rz_low, Rz_high,specrefl_fac
   vref = sqrt(p.Tref/p.mass)

   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1

   if first
      M_left, M_right, b_left, b_right, om_left, om_right = buildBoundaryMatrices(p)
   end

   if zidx == 1
      Min = M_left
      b = b_left
      om = om_left
   elseif zidx == 2
      Min = M_right
      b = b_right
      om = om_right
   else
      error("zidx $zidx not valid in getBoundaryCoeffsFromPreviousFlux.")
   end

   # Add the boundary source
   #flux_integral = p.bndy_z_sourceflux[zidx]
   # no longer doing this here
   #f_in = (Min\(flux_integral*b))

   # Obtain int[ |v_z| * f(v) ; outward halfspace]
   if p.bndy_conds_z[zidx] == "refl_diffuse"
      flux_integral = p.bndy_z_R[zidx]*vref^4*sum(om.*f_prev)
      f_in = (Min\(flux_integral*b))
   elseif p.bndy_conds_z[zidx] == "refl_specular"
      f_in = p.bndy_z_R[zidx]*specularReflectionCoeffs(f_prev,p,first)
   end

   return f_in

end

function getWallBoundary(p::paramData,Mfull::Matrix,zidx::Int64)
   n = p.NLaguerre*p.NLegendre^2
   Lmax = p.NLegendre-1

   M_left, M_right, b_left, b_right, om_left, om_right = buildBoundaryMatrices(p)

   # An alternate set of parameters that uses a different reference temperature for building a convenient mass matrix
   p_alt = deepcopy(p)
   p_alt.Tref = p.bndy_z_temp[zidx]

   Malt,dummy1,dummy2 = buildMassMatrix(p_alt)

   if zidx == 1
      Min = M_left
      b = b_left
      om = om_left
   elseif zidx == 2
      Min = M_right
      b = b_right
      om = om_right
   else
      error("zidx $zidx not valid in getWallBoundary.")
   end

   # The A matrix (in conjunction with the vector y) encodes various conditions on g,
   # the function representing the velocity distribution at the wall.
   A = zeros(n,n)
   y = zeros(n)

   A = deepcopy(Mfull)
   y[:] = M_alt[:,1]

   # First condition: ensure I-g*om^T has exactly one zero-eigenvalue
   i = get_idx(0,1,0,Lmax)
   A[i,:] .= om[:] 
   y[i] = 1.0

   # Second condition: ensure f = f_out + f_in has vanishing flux moment normal to the wall
   # This corresponds to g(v) having unity vz moment when integrating over inward halfspace
   i = get_idx(0,1,0,Lmax)
   A[i,:] .= Min[i,:]
   y[i] = sqrt(3.0/(4.0*pi))

   # Build a basis around the Maxwellian at the wall conditions (except density)
   # Use these to specify the spectral coefficients of g(v) using the corresponding mass matrix
   # TODO
   # Issue: every one of these moments will be proportional to density, which is to be determined 
   # from the eigenvalue constraint

   # Next conditions: ensure g(v) has the appropriate moments specified by the boundary condition, when
   # integrated over all of velocity space, as if it were a Maxwellian with the same moment parameters.
   #
   # vx(wall)
   i = get_idx(0,1,1,Lmax)
   A[i,:] .= Mfull[i,:]
   y[i] = -sqrt(3.0/(4.0*pi))*p.bndy_z_vx[zidx]
   
   # vy(wall)
   i = get_idx(0,1,-1,Lmax)
   A[i,:] .= Mfull[i,:]
   y[i] = -sqrt(3.0/(4.0*pi))*p.bndy_z_vy[zidx]

   # TODO specify the six second moments:
   # k,l,m = [1,0,0], [0,2,-2...2]
   
   g = A\y

   I = diagm(0=>ones(n))

   vals = eigvals(I-g*transpose(om))
   vecs = eigvecs(I-g*transpose(om))

   # Matrix elements are not exact due to roundoff error in calculating various integrals
   # Use the smallest real value instead
   i = argmin(abs(real(vals)))
   fnorm = real(vecs[:,i])
   # TODO: check other eigenvalues are close to unity and 
   # the imaginary part of chosen eigenvector is close to zero

   # Now f is normalized to be a unit vector. Use the specified boundary density to un-normalize
   idens = get_idx(0,0,0,Lmax)
   dens_norm = sqrt(0.25/pi)*Mfull[idens,:]*fnorm
   f_in = fnorm * p.bndy_z_dens[zidx] / dens_norm

   # Now f has the right density,temperature and vanishing vz moment
   # TODO: check these

   return f_in
end

function test_boundaryMatrices()
   p = paramData(3,3,38,8,[],[],[],[],[],[],
                 [],[],"", 0.0, 0.0, false,false,false,
                 10,1.0,1,"euler", [1.0],[0.0],[0.0],[0.0],[1.0],1.0,0.0,1.0,1.0,-1,
                 16,1.0,["refl_diffuse","refl_diffuse"],[1.0,1.0],[0.0,0.0],[0.0,0.0],[0.0,0.0],[0.7,1.3],[1.0,1.0],[0.0,0.0])

   Lmax = p.NLegendre-1
   n = p.NLaguerre*p.NLegendre^2

   M_left, M_right, b_left, b_right, om_left, om_right = buildBoundaryMatrices(p)

   # Sample cases to try
   ks = [0,1,1,1,2,2]
   ls = [2,0,1,1,2,2]
   ms = [-1,0,1,0,-2,0]

   @testset "Testing boundary integrals" begin
      for is in 1:length(ks)
         ki = ks[is]
         li = ls[is]
         mi = ms[is]
         i = get_idx(ki,li,mi,Lmax)
         for js in 1:length(ks)
            kj = ks[js]
            lj = ls[js]
            mj = ms[js]
            j = get_idx(kj,lj,mj,Lmax)

            function Mtest_integrand(v)
               return basis_function(v,ki,li,mi) * basis_function(v,kj,lj,mj)
            end
            numerical = integrate_half_vspace(Mtest_integrand,1)
            result = M_left[i,j]
            @test result ≈ numerical atol=1.0e-8
            numerical = integrate_half_vspace(Mtest_integrand,-1)
            result = M_right[i,j]
            @test result ≈ numerical atol=1.0e-8
         end

         function omtest_integrand(v)
            return basis_function(v,ki,li,mi)*v[1]*abs(cos(v[2]))
         end
         num = integrate_half_vspace(omtest_integrand,-1)
         @test om_left[i] ≈ num atol=1.0e-8

         num = integrate_half_vspace(omtest_integrand,1)
         @test om_right[i] ≈ num atol=1.0e-8

         function bltest_integrand(v)
            return basis_function(v,ks[is],ls[is],ms[is])*exp(-0.5*(v[1]^2)/p.bndy_z_temp[1])
         end
         numerical = integrate_half_vspace(bltest_integrand,1)/(2.0*pi*p.bndy_z_temp[1]^2)
         result = b_left[i]
         @test result ≈ numerical atol=1.0e-8

         function brtest_integrand(v)
            return basis_function(v,ks[is],ls[is],ms[is])*exp(-0.5*(v[1]^2)/p.bndy_z_temp[2])
         end
         numerical = integrate_half_vspace(brtest_integrand,-1)/(2.0*pi*p.bndy_z_temp[2]^2)
         result = b_right[i]
         @test result ≈ numerical atol=1.0e-8
      end
   end;


#   A_left = M_left - b_left*transpose(om_left)
#   A_right = M_right - b_right*transpose(om_right)

#   return A_left, A_right
end
