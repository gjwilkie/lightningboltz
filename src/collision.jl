# Data type defining the parameters that make up a collisional interaction
# Format of label/cross-section file: spec_targetSpec_diffStr_collType.db.collection
#  ("diffStr_type" can be replaced by "all" to read the whole file and use several collision operators)
struct xsecData
   label::String                  # A string encoding all information about the cross section. 
   spec::String                   # The active species
   targetSpec::String             # Target species
   differential::Bool             # If true, use scattering-angle-dependent differential cross section
   collType::String                   # Type of interaction: "elastic", "excitation", "ionization"
   db::String                     # Database the data comes from. e.g. "IST"
   collection::String             # Meta-database e.g. "lxcat"
   charge::Float64                # Charge of the main species
   targetCharge::Float64          # Charge of the target species
   mass::Float64                  # Mass of the main species
   targetMass::Float64            # Mass of the target species
   xsec_table::Array{Float64,2}   # Table of cross sections as read by file
   energy_table::Array{Float64,1} # Energies corresponding to first dimension of xsec_table
   angle_table::Array{Float64,1}  # Scattering angles corresponding to second dimension of xsec_table for differential cross sections.
   logInterp::Bool                # If true, interpolates a cubic spline of the log(sigma). Otherwise, use linear interpolation.
   interpSpline::Spline2D         # A spline object for calculating cross section from tabulated data
end

struct collData
   label::String                  # A string encoding all information about the collision operator. 
   xsec::xsecData                 # xsecData for the cross section the collision operator was built from
   Tref::Float64                  # Reference temperature at which the basis is expanded about
   targetTemp::Float64            # Temperature of the target
   nonlinear::Bool                # If true, the collision operator is nonlinear. Must be consistent with self-collisions.
   elastic::Bool                  # If true, the collision is considered elastic. Must be consistent with "collData.collType".
   C::Array{Float64,3}            # The collision operator for this interaction.
   NLaguerre::Int64               # Resolution paramter used for calculating for collData.C
   NLegendre::Int64               # Resolution paramter used for calculating for collData.C
   NGauss::Int64                  # Resolution paramter used for calculating for collData.C
   NSphere::Int64                 # Resolution paramter used for calculating for collData.C
   sigQuad::Bool                  # If true, the cross section is used to generate the quadrature rule.   
end

function initializeXsec(label_in::String,logInterp=true)

   dummy,db,collection = parse_collstring(label_in,".")

   spec, targetSpec, diffStr, collType = parse_collstring(label_in,"_",".")

   differential = false
   if diffStr == "diff"
      differential = true
   else

   if spec == "e"
      charge = -1.602e-19
      mass = 9.109e-31
   else
      error("xsecData.spec = $spec not recognized.")
   end

   if targetSpec == "Ar"
      targetCharge = 0.0
      targetMass = 40*1.602e-27
   else
      error("xsecData.targetSpec = $targetSpec not recognized.")
   end

   datadir = "../data/xsecs/"
   filename = datadir * label_in
   data = readdlm(filename)

   if collection == "lxcat"
      if differential
         length(data[1,:]) == 3  || error("Inconsistent file naming for $filename. Must have three columns.")

         # TODO: Note this assumes that in the file, the table has the same number of angles for each energy and vice versa. Insert a sanity check on this.
         
         ndata = length(data[:,1])
         nangle = findfirst( data[:,2] .!= data[1,2] ) - 1
         nenergy = ndata/nangle


         angle_table = data[1:nangle,1]*pi/180.0
         energy_table = data[1:nangle:ndata,2] * 1.602e-19

         xsec_table = zeros(nenergy,nangle)
         for i in 1:ndata
            iangle = (i-1)%nangle + 1
            ienergy = div(i-1,nangle) + 1
            xsec_table[ienergy,iangle] = data[i,3]
         end
      else
         energy_table = data[:,1] *1.602e-19   # Convert from eV
         angle_table = -1.0*ones(length(energy_table))
         xsec_table = data[:,2]
      end

   else 
      error("Collection $collection in $label_in not recognized.")
   end

   if logInterp
      logXsec = log.(xsec_table)
      logXsec[ logXsec .<= 0.0 ] .= log(1.0e-40)
      spline = Spline2D(logEn, angle_table, logXsec,k=3,bc="nearest")
   else
      spline = Spline2D(energy_array, angle_table, xsec_table,k=1,bc="nearest")
   end

   coll= xsecData(label,spec,targetSpec,differential,collType,db,collection,charge,targetCharge,mass,targetMass,xsec_table,energy_table,angle_table,logInterp,spline)

end


function xsec_func(c::xsecData,vrel::Float64,angle=-1.0)
   energy = 0.5*c.mass*vrel^2
   xsec = evaluate(c.spline,energy,angle)
   if c.logInterp
      xsec = exp(xsec)
   end
   return xsec
end

function process_input_collstring(label_in::String)
   idx = findfirst(".",label_in)

   if label_in[idx-3:idx-1] == "all"
      # TODO Insert appropriate logic here
      error("Not ready to read whole LXCat file yet.")
   else
      initialCollParams(label_in)
   end
end

function parse_collstring(str::String,delim::String,term="end")
   if term == "end" 
      tempstr = str[1:end]
   else
      idx = findfirst(term,str)
      tempstr = str[1:idx-1]
   end

   res = Array{String,1}
   idx = findfirst(delim,tempstr)
   while idx != nothing
      push!(res, tempstr[1:idx-1])
      tempstr = tempstr[idx+length(delim):end]
      idx = findfirst(delim,tempstr)
   end
   push!(res,tempstr)

   return tuple(res...)
end
