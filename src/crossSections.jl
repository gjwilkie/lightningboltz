
# Obtains post-collision velocities given the pre-collision velocities
# sign = +/- 1 for vprime and wprime, respectively 
@everywhere function vprime(v::Vector,w::Vector,theta::Float64,phi::Float64,sign::Float64)
   unit = spherical2cartesian([1.0, theta, phi])
   vc = spherical2cartesian(v)
   wc = spherical2cartesian(w)

   vp_c = (0.5*(vc+wc) + 0.5*sign*norm(vc-wc) * unit )

   return cartesian2spherical(vp_c)
end

# Inelastic version with restitution coefficient alpha
@everywhere function vprime(v::Vector,w::Vector,theta::Float64,phi::Float64,alpha::Float64,sign::Float64)
   unit = spherical2cartesian([1.0, theta, phi])
   vc = spherical2cartesian(v)
   wc = spherical2cartesian(w)

   vp_c = (0.5*(vc+wc) + sign*0.25*(1.0-alpha)*(vc-wc) + sign*0.25*(1+alpha)*sqrt(sum((vc-wc).^2)) * unit )

   return cartesian2spherical(vp_c)
end
#
# Post-collision velocities for particle colliding against stationary target
@everywhere function vprime_stationary(v::Vector,theta::Float64,phi::Float64,massratio::Float64,alpha::Float64)
#   unit = spherical2cartesian([1.0, theta, phi])
#   vc = spherical2cartesian(v)

   vp_mag = alpha*v[1]*(massratio*cos(theta)+sqrt(1.0-massratio^2*sin(theta)^2))/(1.0+massratio)
#
#   return cartesian2spherical(vp_c)
   return [vp_mag,theta,phi]
end


# Inelastic version with given energy loss
#@everywhere function vprime(v::Vector,w::Vector,theta::Float64,phi::Float64,sign::Float64;Eloss=0.0)
#   unit = spherical2cartesian([1.0, theta, phi])
#   vc = spherical2cartesian(v)
#   wc = spherical2cartesian(w)
#   Utot = sum(vc.^2 + wc.^2)
#   Ucm = sum( (vc+wc).^2)
#
#   alpha = getalpha(Utot,Ucm,Eloss)
#   vp_c = (0.5*(vc+wc) + sign*0.25*(1.0-alpha)*(vc-wc) + sign*0.25*(1+alpha)*norm(vc-wc) * unit )
#
#   return cartesian2spherical(vp_c)
#end



@everywhere function kernel_maxwPseudo(v::Vector,w::Vector,theta::Float64,phi::Float64)
   return 0.25/pi
end

@everywhere function kernel_hardsphere(v::Vector,w::Vector,theta::Float64,phi::Float64)
   vrel = spherical2cartesian(v) - spherical2cartesian(w)
   return sqrt(vrel[1]^2 + vrel[2]^2 + vrel[3]^2) *0.25/pi
end

@everywhere function kernel_coulomb(v::Vector,w::Vector,theta::Float64,phi::Float64,vminparam::Float64)
   unit = spherical2cartesian([1.0, theta, phi])
   vrel = spherical2cartesian(v) - spherical2cartesian(w)
   vnorm = norm(vrel)
   if vnorm < 1.0e-6
      sth_half_sq = 0.0
   else
      vunit = vrel/vnorm
      cth = min(1.0,dot(vunit,unit))
      cth = max(-1.0,cth)
      sth_half_sq = 0.5*(1.0-cth)
   end

   return vnorm/(vminparam + vnorm^2*sth_half_sq)^2
end

@everywhere function kernel_chargeExchH(v::Vector,w::Vector,theta::Float64,phi::Float64,Tref::Float64)
   vrel = spherical2cartesian(v) - spherical2cartesian(w)
   E = 0.5*1.66054e-27*sum(vrel.^2)

   E = max(E,1.0e-21)

   E = E/(1.602e-19*1e3)

   A1 = 3.2345
   A2 = 2.3588e2
   A3 = 2.3713
   A4 = 3.8371e-2
   A5 = 3.8068e-6
   A6 = 1.1832e-10


   return (0.25/pi)*norm(vrel)*1.0e-20*A1*max(0.0,log(A3 + A2/E)) / (1.0 + A4*E + A5*E^1.5 + A6*E^5.4)
end

@everywhere function kernel_exciteH(v::Vector,w::Vector,theta::Float64,phi::Float64,Tref::Float64)
   vrel = spherical2cartesian(v) - spherical2cartesian(w)
   E = 0.5*sum(vrel.^2)

   # Use 0.01eV as a minimum value (without blowing up)
   E = max(E,1.0e-21)

   E = E/(1.602e-19*1e3)

   a1 = 34.433
   a2 = 8.5476
   a3 = 7.8501
   a4 = -9.2217
   a5 = 1.8020e-2
   a6 = 1.6931
   a7 = 1.9422e-3
   a8 = 2.9068
   a9 = 44.507
   a10 = .56870

   return (0.25/pi)*norm(vrel)*a1*( a2*exp(-a3*E)*E^(-a4) + a5*exp(-a6/E)/(1.0+a7*E^a8) + exp(-a9/E)*log(1.0+a10*E)/E ) 

end

@everywhere function kernel_lxcat_diff(v::Vector,w::Vector,theta::Float64,phi::Float64,func)

   urel = norm(spherical2cartesian(v) - spherical2cartesian(w))

   return urel*func(urel,theta)
end


@everywhere function kernel_lxcat(v::Vector,w::Vector,theta::Float64,phi::Float64,func)

   urel = norm(spherical2cartesian(v) - spherical2cartesian(w))

   return (0.25/pi)*urel*func(urel)
end


@everywhere function kernel_diff_lxcat(v::Vector,w::Vector,theta::Float64,phi::Float64,func)

   urel = norm(spherical2cartesian(v) - spherical2cartesian(w))

#   return urel*func(urel,abs(theta-v[2]))
   return urel*func(urel,theta)
end


@everywhere function kernel_degas2(v::Vector,w::Vector,theta::Float64,phi::Float64,func)

   urel = norm(spherical2cartesian(v) - spherical2cartesian(w))

   log_specific_energy = log(0.5*urel^2)
   return (0.25/pi)*urel*exp(func(log_specific_energy))
end

@everywhere function alphaconst(v::Vector,w::Vector,alpha::Float64)
   return alpha
end

# Obtains velocity-dependent restitution coefficient given energy loss
# Utot = norm(vp)^2 + norm(wp)^2, post-collision speeds
# Ucm = norm(v+w)^2
# Eloss = kinetic energy lost in inelastic collision
@everywhere function getalpha(Utot::Float64,Ucm::Float64,Eloss::Float64,mass::Float64)
   if (Utot < 0.5*Ucm)
      error("Utot = $Utot, Ucm = $Ucm")
   end
   return sqrt( (Utot - 0.5*Ucm)/(Utot - 0.5*Ucm + 2.0*Eloss/mass) )
end

@everywhere function getalpha_exch(v::Vector,w::Vector)
   return sqrt(sum(2*w.^2)/sum(v.^2+w.^2))
end
