# Data type for storing basic simulation parameters
@everywhere mutable struct paramData
   NLaguerre::Int64  # K
   NLegendre::Int64  # L
   NSphere::Int64    # NL
   NGauss::Int64     # NGL

   collModel::Array{String,1}
   collParams::Array{Float64,1}

   linCollModel::Array{String,1}
   linCollParams::Array{Float64,1}
   targetMass::Array{Float64,1}
   targetTemp::Array{Float64,1}

   xsQuad::Bool

   sourceModel::Array{String,1}
   sinkModel::Array{String,1}

   backgroundModel::String

   Ez::Float64
   Efreq::Float64

   useLocalMatrixFile::Bool
   downloadMatrixFile::Bool
   saveMatrixFile::Bool

   Nout::Int64

   dt::Float64
   Ntime::Int64
   timeMethod::String # "euler","rk2","rk4"

   initDens::Array{Float64,1}
   initVx::Array{Float64,1}
   initVy::Array{Float64,1}
   initVz::Array{Float64,1}
   initTemp::Array{Float64,1}
   Tref::Float64
   charge::Float64
   mass::Float64
   sigref::Float64

   nNewtonIter::Int64

   Nz::Int64
   Lz::Float64
   bndy_conds_z::Array{String,1}
   bndy_z_dens::Array{Float64,1}
   bndy_z_Vx::Array{Float64,1}
   bndy_z_Vy::Array{Float64,1}
   bndy_z_Vz::Array{Float64,1}
   bndy_z_temp::Array{Float64,1}
   bndy_z_R::Array{Float64,1}
   bndy_z_sourceflux::Array{Float64,1}
   bndy_z_vexp::Array{Int64,1}
end

function processInputs_json(file::String)
   d = JSON.parsefile(file) 

   # Check for mandatory inputs
   mandatorykeys = ["NLaguerre","NLegendre","NSphere","NGauss","dt","Ntime","collModel"]
   for key in mandatorykeys
      try d[key] catch; error("Must define $key in input file.\n") end
   end

   # Defaults
   try d["collModel"] catch; d["collModel"] = [] end
   try d["collParams"] catch; d["collParams"] = ones(length(d["collModel"])) end
   try d["linCollModel"] catch; d["linCollModel"] = [] end
   try d["linCollParams"] catch; d["linCollParams"] = ones(length(d["linCollModel"])) end
   try d["targetMass"] catch; d["targetMass"] = Inf*ones(length(d["linCollModel"])) end
   try d["targetTemp"] catch; d["targetTemp"] = zeros(length(d["linCollModel"])) end
   try d["xsQuad"] catch; d["xsQuad"] = false end
   try d["useLocalMatrixFile"] catch; d["useLocalMatrixFile"] = true end
   try d["downloadMatrixFile"] catch; d["downloadMatrixFile"] = true end
   try d["saveMatrixFile"] catch; d["saveMatrixFile"] = true end
   try d["Nout"]  catch; d["Nout"] = d["Ntime"] end
   try d["timeMethod"]  catch; d["timeMethod"] = "rk4" end
   try d["backgroundModel"]  catch; d["backgroundModel"] = "none" end
   try d["Ez"]  catch; d["Ez"] = 0.0 end
   try d["Efreq"]  catch; d["Efreq"] = 0.0 end
   try d["initDens"]  catch; d["initDens"] = 1.0 end
   try d["initVx"]  catch; d["initVx"] = 0.0 end
   try d["initVy"]  catch; d["initVy"] = 0.0 end
   try d["initVz"]  catch; d["initVz"] = 0.0 end
   try d["initTemp"]  catch; d["initTemp"] = 1.0 end
   try d["Tref"]  catch; d["Tref"] = 1.0 end
   try d["charge"]  catch; d["charge"] = 1.0 end
   try d["mass"]  catch; d["mass"] = 1.0 end
   try d["sigref"]  catch; d["sigref"] = 1.0 end
   try d["nNewtonIter"]  catch; d["nNewtonIter"] = 0 end
   try d["Nz"]  catch; d["Nz"] = 1 end
   try d["Lz"]  catch; d["Lz"] = 1.0 end
   try d["bndy_conds_z"]  catch; d["bndy_conds_z"] = ["dirichlet", "dirichlet"] end
   try d["bndy_z_dens"] catch; d["bndy_z_dens"] = ones(Float64,2) end
   try d["bndy_z_Vx"] catch; d["bndy_z_Vx"] = zeros(Float64,2) end
   try d["bndy_z_Vy"] catch; d["bndy_z_Vy"] = zeros(Float64,2) end
   try d["bndy_z_Vz"] catch; d["bndy_z_Vz"] = zeros(Float64,2) end
   try d["bndy_z_temp"] catch; d["bndy_z_temp"] = ones(Float64,2) end
   try d["bndy_z_R"] catch; d["bndy_z_R"] = ones(Float64,2) end
   try d["bndy_z_sourceflux"] catch; d["bndy_z_sourceflux"] = zeros(Float64,2) end
   try d["bndy_z_vexp"] catch; d["bndy_z_vexp"] = ones(Float64,2) end

   # TODO: Better relationship between initDens, etc and BCs

   # Convert dictionary to struct
   NLaguerre = d["NLaguerre"] 
   NLegendre = d["NLegendre"] 
   NSphere = d["NSphere"] 
   NGauss = d["NGauss"] 
   collModel = d["collModel"]
   sourceModel = d["sourceModel"]
   linCollModel = d["linCollModel"]
   linCollParams = d["linCollParams"]
   targetMass = d["targetMass"]
   targetTemp = d["targetTemp"]
   xsQuad = d["xsQuad"]
   sinkModel = d["sinkModel"]
   backgroundModel = d["backgroundModel"]
   Ez = d["Ez"] 
   Efreq = d["Efreq"] 
   collParams = convert(Array{Float64,1},d["collParams"] )
   useLocalMatrixFile = d["useLocalMatrixFile"]
   downloadMatrixFile = d["downloadMatrixFile"]
   saveMatrixFile = d["saveMatrixFile"]
   Nout = d["Nout"]
   dt = d["dt"]
   Ntime = d["Ntime"]
   timeMethod = d["timeMethod"]
   initDens = convert(Array{Float64,1},d["initDens"] )
   initTemp = convert(Array{Float64,1},d["initTemp"] )
   initVx = convert(Array{Float64,1},d["initVx"] )
   initVy = convert(Array{Float64,1},d["initVy"] )
   initVz = convert(Array{Float64,1},d["initVz"] )
   Tref = d["Tref"]
   charge = d["charge"]
   mass = d["mass"]
   sigref = d["sigref"]
   nNewtonIter = d["nNewtonIter"]
   Nz = d["Nz"]
   Lz = d["Lz"]
   bndy_conds_z=  d["bndy_conds_z"] 
   bndy_z_dens = d["bndy_z_dens"]
   bndy_z_Vx = d["bndy_z_Vx"]
   bndy_z_Vy = d["bndy_z_Vy"]
   bndy_z_Vz = d["bndy_z_Vz"]
   bndy_z_temp = d["bndy_z_temp"]
   bndy_z_R = d["bndy_z_R"]
   bndy_z_sourceflux = d["bndy_z_sourceflux"]
   bndy_z_vexp = d["bndy_z_vexp"]


   # Sanity check
   check = (length(initVx) != length(initVy)) 
   check = check || (length(initVx) != length(initVz))
   check = check || (length(initVx) != length(initDens))
   check = check || (length(initVx) != length(initTemp))
   check &&  error("Initialization parameters must all be the same size array to initialize a sum of Maxwellians.")

   if (nNewtonIter > 0) && timeMethod != "backEuler"
      @warn "nNewtonIter = $nNewtonIter, but explicit timeMethod $timeMethod was chosen. Ignoring nNewtonIter."
      nNewtonIter = 0
   elseif (nNewtonIter < 1) && timeMethod == "backEuler"
      @warn "nNewtonIter not specified for implicit timestepping. Setting equal to 1."
      nNewtonIter = 1
   end

   if (Nz == 1)
      bndy_conds_z = ["dirichlet","dirichlet"]
   end

   p = paramData(NLaguerre, NLegendre,NSphere,NGauss,collModel,collParams,linCollModel,linCollParams,targetMass,targetTemp,xsQuad,
                 sourceModel,sinkModel, backgroundModel, Ez, Efreq, useLocalMatrixFile,downloadMatrixFile,saveMatrixFile,
                 Nout,dt,Ntime,timeMethod, initDens,initVx,initVy,initVz,initTemp,Tref,charge,mass,sigref,nNewtonIter,
                 Nz,Lz,bndy_conds_z,bndy_z_dens,bndy_z_Vx,bndy_z_Vy,bndy_z_Vz,bndy_z_temp,bndy_z_R,bndy_z_sourceflux,bndy_z_vexp)

   return p
end

