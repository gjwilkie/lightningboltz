function plotDists(f::Matrix,p::paramData)
   vref = sqrt(p.Tref/p.mass)

   Nv = 400
   vmax = 4.0*vref

   vx = collect(range(-vmax,vmax,length=Nv))
   finit = zeros(Nv)
   ffinal = zeros(Nv)

   for ix in 1:Nv
      finit[ix] = get_f([ vx[ix], 0.0,0.0], f[:,1],p)

      ffinal[ix] = get_f([ vx[ix], 0.0,0.0], f[:,end],p)
   end

   plot(vx,finit,"g")
   plot(vx,ffinal,"b")
   xlabel(L"$v_z$",fontsize=18)
   ylabel(L"$f$",fontsize=18)
   legend([L"$f(0)$",L"$f(T)$",L"$f_M$"])
   savefig("tempdist.png")
   cla()
   clf()
   close()
end

function plot_1d_status(f::Array{Float64,2},p::paramData,M::Matrix,it::Int64)
   Lmax=p.NLegendre-1
   density = zeros(p.Nz)
   dz = p.Lz/(p.Nz)
   z_grid = collect(range(0.5*dz,stop=p.Lz-0.5*dz,length=p.Nz))
   for iz in 1:p.Nz
      psi_moments= M*f[:,iz]
      i = get_idx(0,0,0,Lmax)
      density[iz] = sqrt(4*pi)*psi_moments[i]
   end

   plot(z_grid,density)
   xlabel(L"$z$",fontsize=18)
   ylabel(L"$n$",fontsize=18)
   title("Output step $it")
   savefig("nvsz.png")
   cla()
   clf()
   close()
end

function postprocess_1d(fOut::Array{Float64,3},p::paramData,M::Matrix)

   vref = sqrt(p.Tref/p.mass)

   dz = p.Lz/(p.Nz)
   z_grid = collect(range(0.5*dz,stop=p.Lz-0.5*dz,length=p.Nz))

   dens = zeros(p.Nz,p.Nout)
   flow = zeros(p.Nz,3,p.Nout)
   temp = zeros(p.Nz,p.Nout)
   hflux = zeros(p.Nz,p.Nout)
   chi = zeros(p.Nz,p.Nout)
   stress = zeros(p.Nz,3,3,p.Nout)

   for iout in 1:p.Nout
#   for iout in [1,2,p.Nout-1,p.Nout]
#   for iout in [1,p.Nout-1,p.Nout]

      for iz in 1:p.Nz
         n0,u0,T0,E0,M0,r0,s0,q0,dummy = calculate_moments(fOut[:,iz,iout],p)
#         n0,u0,E0 = calculate_moments_direct(M*fOut[:,iz,iout],p)
         dens[iz,iout] = n0
         flow[iz,:,iout] = u0
         temp[iz,iout] = T0
         stress[iz,:,:,iout] = M0
         hflux[iz,iout] = q0
      end
#      gradT = (temp[3:end] - temp[1:end-2])/(2*p.Lz/p.Nz)
#      chi = -hflux[2:end-1]./(dens[2:end-1].*gradT)
#
#      dens[1] = p.bndy_z_dens[1]
#      dens[end] = p.bndy_z_dens[2]
#      
#      flow[1,:] = [p.bndy_z_Vx[1], p.bndy_z_Vy[1], p.bndy_z_Vz[1]]
#      flow[end,:] = [p.bndy_z_Vx[2], p.bndy_z_Vy[2], p.bndy_z_Vz[2]]
#      
#      temp[1] = p.bndy_z_temp[1]
#      temp[end] = p.bndy_z_temp[2]

      
#      plot(z_grid[2:end-1],flow[2:end-1,1],"-+")
#      plot(z_grid,flow,"-+")
#      plot(z_grid[2:end-1],temp[2:end-1],"-+")
#      plot(z_grid,temp,"-+")
#      plot(z_grid[2:end-1],chi,"-+")
#      plot(z_grid,dens,"-+")
   end

   save("moments.jld","dens",dens,"flow",flow,"temp",temp,"hflux",hflux,"stress",stress)

#   vt = sqrt.(2.0*temp/p.mass)
   vref = sqrt.(abs.(temp/p.mass))
   chi_CE = (5*sqrt(pi)/16.0)*(1.0+(1.0/44.0))*4.0*pi*vref./(4.0*dens*p.sigref)
#   chi_CE = .678*(vref./dens)*0.25*pi/(p.sigref)
#   plot(z_grid[2:end-1],chi_CE[2:end-1],"--")


   plot(z_grid,dens)
   legend(["it=1","it=2","it=3","it=4"])
   savefig("densvsz.png")
   clf()

   semilogy(z_grid,abs.(dens))
   legend(["it=1","it=2","it=3","it=4"])
   savefig("logdensvsz.png")
   clf()


   plot(z_grid,flow[:,1,:])
   legend(["it=1","it=2","it=3","it=4"])
   savefig("uxvsz.png")
   clf()

   plot(z_grid,flow[:,3,:])
   legend(["it=1","it=2","it=3","it=4"])
   savefig("uzvsz.png")
   clf()

   plot(z_grid,temp)
   legend(["it=1","it=2","it=3","it=4"])
   savefig("tempvsz.png")
   clf()

   semilogy(z_grid,abs.(dens.*temp))
   legend(["it=1","it=2","it=3","it=4"])
   savefig("log-presvsz.png")
   clf()


   plot(z_grid,hflux)
   legend(["it=1","it=2","it=3","it=4"])
   savefig("hfluxvsz.png")
   clf()

   plot(z_grid,stress[:,1,3,:])
   legend(["it=1","it=2","it=3","it=4"])
   savefig("xzstressvsz.png")
   clf()

   plot(z_grid,stress[:,3,1,:])
   legend(["it=1","it=2","it=3","it=4"])
   savefig("xzstressvsz.png")
   clf()

#   legend(["it=1","it=2","it=3","it=4","it=5"])
#   legend(["it=1","it=2"])
#   savefig("densvsz.png")
#   savefig("tempvsz.png")
#   savefig("hfluxvsz.png")
#   savefig("chivsz.png")
#   savefig("uxvsz.png")
   cla()
   clf()
   close()
 
end 

function postprocess_0d(fOut::Matrix,p::paramData,massMatrix::Matrix)
   nout= zeros(p.Nout)
   uout= zeros(p.Nout)
   uzout= zeros(p.Nout)
   Tout= zeros(p.Nout)
   Eout= zeros(p.Nout)
   M11out= zeros(p.Nout)
   r1out = zeros(p.Nout)
   sout = zeros(p.Nout)
   tOut = zeros(p.Nout)

   vref = sqrt(p.Tref/p.mass)

   n0,u0,T0,E0,M0,r0,s0,q0,dummy = calculate_moments(fOut[:,1],p)

   M11exact = zeros(p.Ntime)
   r1exact = zeros(p.Ntime)
   sexact = zeros(p.Ntime)
   tall = zeros(p.Ntime)

   errorMmax =0.0
   errorsmax =0.0
   iOut = 1

#   nout[1],dummy,dummy2 = calculate_moments_direct(massMatrix*fOut[:,1],p)
#   dummy2,u,Tout[1],Eout[1],M,r,sout[1],q0,dummy = calculate_moments(fOut[:,1],p)
   nout[1],u,Tout[1],Eout[1],M,r,sout[1],q0,dummy = calculate_moments(fOut[:,1],p)
   M11out[1] = M[1,1]
   r1out[1] = r[1]
   uout[1] = u[1]
   uzout[1] = u[3]

   for it in 1:p.Ntime
      t = (it-1)*p.dt
      tall[it] = t
      tref = 1.0/(sum(p.initDens)*p.sigref*vref)
#      tref = 1.0/(sum(p.initDens)*p.sigref)
#      tref = 1.0
M11exact[it],r1exact[it],sexact[it] = exactMomentsVSt(1.0,u0/vref,T0/(p.mass*vref^2),M0/(n0*vref^2),r0/(n0*vref^3),s0/(n0*vref^4),t/tref)
      M11exact[it] = M11exact[it]*n0*vref^2
      r1exact[it] = r1exact[it]*n0*vref^3
      sexact[it] = sexact[it]*n0*vref^4
      if in( it, round.(Int64,range(1, p.Ntime, length=p.Nout-1)) )
         iOut += 1
#         nout[iOut],dummy,dummy2, = calculate_moments_direct(massMatrix*fOut[:,iOut],p)
#         dummy2,u,Tout[iOut],Eout[iOut],M,r,sout[iOut],q0,dummy = calculate_moments(fOut[:,iOut],p)
         nout[iOut],u,Tout[iOut],Eout[iOut],M,r,sout[iOut],q0,dummy = calculate_moments(fOut[:,iOut],p)
         M11out[iOut] = M[1,1]
         r1out[iOut] = r[1]
         uout[iOut] = u[1]
         uzout[iOut] = u[3]
         tOut[iOut] = t

         errorMmax = max(errorMmax,abs( (M11exact[it]-M11out[iOut])/M11exact[it]))
         errorsmax = max(errorsmax,abs( (sexact[it]-sout[iOut])/sexact[it]))
      end
   end

   println("Maximum relative errors = $errorMmax, $errorsmax")
   println("$(nout[1]), $(uout[1]), $(Tout[1]), $(Eout[1])")
   println("$(nout[end]), $(uout[end]), $(Tout[end]), $(Eout[end])")

#   plt[:figure](figsize=(6,3))
#   plt[:subplot](1,2,1)
   plt.figure(figsize=(3,3))
   plot(tall,M11exact,linewidth=1.0,"-k")
   plot(tOut,M11out,linewidth=1.0,"+r")
   xlabel(L"$t $",fontsize=18)
   ylabel(L"$\int v_x^2 f(\mathbf{v})\, \mathrm{d}^3\mathbf{v}$",fontsize=18)
#   xlim(0.0,20.0)
   legend(("Analytic","Numerical"))
   savefig("M11.pdf",bbox_inches="tight")
   clf()
   cla()
   close()

   plt.figure(figsize=(3,3))
   plot(tall,sexact,linewidth=1.0,"-k")
   plot(tOut,sout,linewidth=1.0,"+r")
   xlabel(L"$t$",fontsize=18)
   ylabel(L"$\int v^4 f(\mathbf{v})\, \mathrm{d}^3\mathbf{v}$",fontsize=18)
#   xlim(0.0,20.0)
   legend(("Analytic","Numerical"))
   savefig("s.pdf",bbox_inches="tight")
   clf()
   cla()
   close()

   if length(tOut) > 20
      sty = "-"
   else
      sty = "o-"
   end

   plt.figure(figsize=(3,3))
   plot(tOut,nout,linewidth=1.0,sty)
   xlabel(L"$t $",fontsize=18)
   ylabel(L"$n \mathrm{m}^{-3}$ ",fontsize=18)
#   xlim(0.0,20.0)
   savefig("nvst.pdf",bbox_inches="tight")
   clf()
   cla()
   close()

   plt.figure(figsize=(3,3))
   plot(tOut,uzout,linewidth=1.0,sty)
   xlabel(L"$t $",fontsize=18)
   ylabel(L"$u_z \left(\mathrm{m}/\mathrm{s} \right)$ ",fontsize=18)
#   xlim(0.0,20.0)
   savefig("uzvst.pdf",bbox_inches="tight")
   clf()
   cla()
   close()



   plt.figure(figsize=(3,3))
   plot(tOut,Tout/1.602e-19,linewidth=1.0,sty)
   xlabel(L"$t $",fontsize=18)
   ylabel(L"$T$ (eV)",fontsize=18)
#   xlim(0.0,20.0)
   savefig("Tvst.pdf",bbox_inches="tight")
   clf()
   cla()
   close()


   Nv = 400
   vmax = 4.0*vref

   vx = collect(range(-vmax,vmax,length=Nv))
   vy = collect(range(-vmax,vmax,length=Nv))
   finit = zeros(Nv,Nv)
   ffinal = zeros(Nv,Nv)

   for ix in 1:Nv
      for iy in 1:Nv
         finit[ix,iy] = get_f([vx[ix], vy[iy], 0.0], fOut[:,1],p)
         ffinal[ix,iy] = get_f([vx[ix], vy[iy], 0.0], fOut[:,end],p)
      end
   end
   zmax = maximum(finit)
   zmax = max(zmax,maximum(ffinal))
   finit = finit'
   ffinal = ffinal'

   plt.figure(figsize=(4,4))
   surf(vy,vx,finit,facecolors=get_cmap("GnBu")(finit/zmax),rstride=4,cstride=4)
   xlabel(L"$v_x $",fontsize=16)
   ylabel(L"$v_y $",fontsize=16)
#   zlabel(L"$f\left(v_x,v_y,v_z=0\right)$",fontsize=12)
   savefig("finit.pdf",bbox_inches="tight")
   clf()
   cla()
   close()

   plt.figure(figsize=(4,4))
   surf(vy,vx,ffinal,facecolors=get_cmap("GnBu")(ffinal/zmax),rstride=4,cstride=4)
   xlabel(L"$v_y $",fontsize=16)
   ylabel(L"$v_x $",fontsize=16)
#   zlabel(L"$f\left(v_x,v_y,v_z=0\right)$",fontsize=12)
   savefig("ffinal.pdf",bbox_inches="tight")
   clf()
   cla()
   close()



   Nv = 100
   vmax = 4.0*vref

   vz = range(-vmax,vmax,length=Nv)
   vx = range(-vmax,vmax,length=Nv)
   finit = zeros(Nv)
   ffinal = zeros(Nv)

   for iv in 1:Nv
#      finit[iv] = get_f([0.0, 0.0, vz[iv]], fOut[:,1],p)
#      ffinal[iv] = get_f([0.0, 0.0, vz[iv]], fOut[:,end],p)
      finit[iv] = get_f([vx[iv], 0.0, 0.0], fOut[:,1],p)
      ffinal[iv] = get_f([vx[iv], 0.0, 0.0], fOut[:,end],p)
   end

   clf()
   cla()
   plot(vz,finit)
   plot(vz,ffinal)
   legend(("Initial","Final"))
   savefig("fcomp.pdf",bbox_inches="tight")
   cla()
   clf()
   close()

   ### Save output data
end

# Get first few moments directly from mass matrix without expensive integration
function calculate_moments_direct(psi_moments::Vector,p::paramData)
   Lmax=p.NLegendre-1
   i = get_idx(0,0,0,Lmax)
   n= sqrt(4*pi)*psi_moments[i]/basisnormfac(0,0)

   i = get_idx(0,1,0,Lmax)
   vz =sqrt(4*pi/3)*psi_moments[i]/basisnormfac(0,1)
   i = get_idx(0,1,1,Lmax)
   vx = -sqrt(4*pi/3)*psi_moments[i]/basisnormfac(0,1)
   i = get_idx(0,1,-1,Lmax)
   vy = -sqrt(4*pi/3)*psi_moments[i]/basisnormfac(0,1)

   i= get_idx(1,0,0,Lmax)
   En = -sqrt(4*pi)*(psi_moments[i]-1.5)/basisnormfac(1,0)

   return n,[vx,vy,vz],En
end

# Get moments given coefficients for diagnostic purposes
function calculate_moments(fin::Vector,p::paramData)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb, vs, vsnorm

   dens = 0.0
   pres = 0.0
   u = zeros(3)
   T = 0.0
   r = zeros(3)
   M = zeros(3,3)
   q = 0.0
   pi_xz = 0.0
   s = 0.0
   En = 0.0

   vref = sqrt(p.Tref/p.mass)

   f = fin
   n = length(f)

   NL = p.NSphere
   NGL = p.NGauss

   for i in 1:n
      k,l,m = get_idx(i,p.NLegendre-1)
      for iv in 1:NGL
         for jv in 1:NL
            dens += f[i] *w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)

            u += vc[:,iv,jv]*f[i] *w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)

            En += p.mass*vs[1,iv,jv]^2*f[i] *w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1]^2/vref^2)

            M += vc[:,iv,jv]*transpose(vc[:,iv,jv])*f[i]*w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)
            r += vc[:,iv,jv]*vs[1,iv,jv]^2*f[i]*w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)
            s += vs[1,iv,jv]^4 *f[i]*w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)
         end
      end
   end

   u = u/dens

   for i in 1:n
      k,l,m = get_idx(i,p.NLegendre-1)
      for iv in 1:NGL
         for jv in 1:NL
            pres += (1.0/3.0)*p.mass*sum((vc[:,iv,jv]-u).^2) *f[i]*w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)
            q += 0.5*p.mass*sum((vc[:,iv,jv]-u).^2)*(vc[3,iv,jv]-u[3]) *f[i]*w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)
            pi_xz += (1.0/3.0)*p.mass*((vc[1,iv,jv]-u[1])*(vc[3,iv,jv])-u[3]) *f[i]*w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)

         end
      end
   end

   T = pres/dens

   return dens, u, T, En, M,r, s, q, pi_xz
end

# Get moments given coefficients for diagnostic purposes
function calculate_entropy(f::Vector,p::paramData)
   global x_gm, w_gm, theta_leb, phi_leb, w_leb, vs, vsnorm

   S = 0.0

   n = length(f)

   NL = p.NSphere
   NGL = p.NGauss

   for i in 1:n
      k,l,m = get_idx(i,p.NLegendre-1)
      for iv in 1:NGL
         for jv in 1:NL
            S += -f[i]*log(abs(f[i]))*w_leb[jv]*w_gm[iv]*basis_function(vsnorm[:,iv,jv],k,l,m)*exp(0.5*vs[1,iv,jv]^2/vref^2)

         end
      end
   end

   return S
end



