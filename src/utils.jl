function getMatrixID(p::paramData,i::Int64)
   s = p.collModel[i]*"__"
   s = s*string(p.Tref)*"__"
   s = s*string(p.NLaguerre)*"__"*string(p.NLegendre)*"__"
   s = s*string(p.NSphere)*"__"*string(p.NGauss)
   if p.xsQuad
      s = s*"__cx_"*string(p.sigref)
   end
   return s
end

@everywhere function cartesian2spherical(vc::Array{Float64,1})
   vs = zeros(Float64,3)
#   vs[1] = norm(vc)
   vs[1] = sqrt(vc[1]^2 + vc[2]^2 + vc[3]^2)
   vs[2] = acos( vc[3]/vs[1])
   vs[3] = atan( vc[2],vc[1] )

   if (isinf(vs[2]) || isnan(vs[2]))
      vs[2] = 0.0
   end

   if (isinf(vs[3]) || isnan(vs[3]))
      if vc[2] < 0.0
         vs[3] = -0.5*pi
      else
         vs[3] = 0.5*pi
      end
   end
   return vs
end

@everywhere function spherical2cartesian(vs::Vector)
   return[ vs[1]*sin(vs[2])*cos(vs[3]), vs[1]*sin(vs[2])*sin(vs[3]), vs[1]*cos(vs[2])]
end

function fM(dv::Vector,vt2::Float64)
   return (2*pi*vt2)^(-1.5) * exp( - 0.5*sum(dv.^2)/vt2)
end

function fMaxw(v::Vector,n::Float64,u::Vector,vt2::Float64)
   return n*(2*pi*vt2)^(-1.5) * exp( - 0.5*sum((v-u).^2)/vt2)
end

@everywhere function fMaxws(v::Vector,n::Float64,u::Vector,vt2::Float64)
   vc = spherical2cartesian(v)
   return n*(2*pi*vt2)^(-1.5) * exp( - 0.5*sum((vc-u).^2)/vt2)
end


