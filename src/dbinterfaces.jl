@everywhere degas2dir = "/Users/gwilkie/src/degas2/data"
@everywhere lxcatdir = "$(@__DIR__)/../data/xsecs"

@everywhere function get_lxcat_data(filename::String)
   me = 9.1094e-31
   data = readdlm(filename)
   energy_dat = data[:,1]*1.602e-19
   xsec_dat = data[:,2]

   u_dat = sqrt.(2.0*energy_dat/me)

   interpolant = Spline1D(u_dat,xsec_dat,k=1)

   return interpolant
end


@everywhere function get_diff_lxcat_data(filename::String)
   me = 9.1094e-31
   data = readdlm(filename)
   theta_dat = data[:,1]*pi/180.0
   energy_dat = data[:,2]*1.602e-19
   xsec_dat = data[:,3]

   u_dat = sqrt.(2.0*energy_dat/me)

   # Assumes that scattering angle has a resolution of one degree
   theta_dat = theta_dat[1:181]
   u_dat = u_dat[1:181:end]

   xsec_dat = transpose(reshape(xsec_dat,(181,length(u_dat))))

   interpolant = Spline2D(u_dat,theta_dat,xsec_dat)

   return interpolant
end

# Usage example:
#  xsec_func = get_degas2_datatable("hh_05_elastic.nc","cross_section")
#  cross_section = xsec_func(rel_energy)
@everywhere function get_degas2_data(filename::String,quantity::String)

   max_indvars = length( ncread(filename,"xs_tab_index")[:,1])
   max_vars = max_indvars+1

   varnames = nc_char2string(ncread(filename,"xs_var"))

   var_idx = -1
   for ivar in 1:max_vars:length(varnames)
      if strip(varnames[ivar]) == strip(quantity)
         var_idx = ivar
      end
   end
   var_idx > 0 || error("Could not find variable $quantity in file $filename")

   rank = ncread(filename,"xs_rank")[var_idx]
   data_start = ncread(filename,"xs_data_base")[var_idx]+1
   array_size = []
   indepvar_table = []
   indepvar_log = []
   ndata_tot = 1
   for idim in 1:rank
      dimsize = convert(Int64,ncread(filename,"xs_tab_index")[idim,var_idx])
      multiplier = ncread(filename,"xs_mult")[idim+1,var_idx]
      varmin = ncread(filename,"xs_min")[idim,var_idx]*multiplier
      varmax = ncread(filename,"xs_max")[idim,var_idx]*multiplier
      
      logvar = (strip(nc_char2string(ncread(filename,"xs_spacing"))[idim+1,var_idx]) == "log")
      if logvar
         indepvar = collect(range(log(varmin),stop=log(varmax),length=dimsize))
      else
         indepvar = collect(range(varmin,stop=varmax,length=dimsize))
      end
                           
      push!(indepvar_log,logvar)
      push!(array_size,dimsize)
      push!(indepvar_table,indepvar)
      ndata_tot *= dimsize
   end

   data_flat = ncread(filename,"xs_data_tab")[data_start:data_start+ndata_tot-1]
   data_array = reshape(data_flat,array_size...)
   multiplier = ncread(filename,"xs_mult")[1,var_idx]
   data_array = multiplier*data_array

   log_dep = (strip(nc_char2string(ncread(filename,"xs_spacing"))[1,var_idx]) == "log")
   if log_dep
      data_array = log.(data_array)
   end

   interpolant = interpolate(tuple(indepvar_table...),data_array,Gridded(Linear()))
   interpolant = extrapolate(interpolant,Flat())

   function consolidated_interpolant(x)
      x_array = [x...]

      for i in 1:length(x_array)
         if indepvar_log[i]
            x_array[i] = exp(x[1])
         else
            x_array[i] = x[1]
         end
      end

      x_use = tuple(x_array...)

      if log_dep
         return exp(interpolant(x_use))
      else
         return interpolant(x_use)
      end
   end

#   return consolidated_interpolant
   return interpolant
end
