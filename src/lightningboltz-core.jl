#!/usr/bin/env julia
# A fast spectral solver for the elastic Boltzmann equation 
# Written by George Wilkie (Chalmers U.), January 2018
# Implements the Galerkin-Petrov scheme of Gamba and Rjasanow 2017 ( https://arxiv.org/abs/1710.05903 )
# Lebedev quadrature rules provided by Casper Beentjes (http://people.maths.ox.ac.uk/beentjes/Essays/QuadratureSphere.pdf  )
# Treatment of spatial dependence closely follows Kessler & Rjasanow 2019
using JSON
using JLD
using ProgressMeter
using Distributed
@everywhere using GSL: sf_gamma, sf_legendre_Plm, sf_laguerre_n, sf_legendre_Pl, sf_coupling_3j
using FastGaussQuadrature: gausslaguerre, gausslegendre
using PyPlot
using Cubature
using QuadGK
using PyCall
@everywhere using GenericLinearAlgebra, LinearAlgebra
using IterativeSolvers
using SharedArrays
@everywhere using DelimitedFiles
@everywhere using Dierckx
using Quadmath
@everywhere using NetCDF
@everywhere using Interpolations
#using WignerSymbols
using Test
include("dbinterfaces.jl")
include("params.jl")
include("Genquad.jl")
include("utils.jl")
include("basis.jl")
include("boundary.jl")
include("crossSections.jl")
include("matrix.jl")
include("postprocess.jl")
PyCall.PyDict(matplotlib."rcParams")["text.usetex"] = true
PyCall.PyDict(matplotlib."rcParams")["font.family"] = "serif"

function prepareParams(arg::String)

   if length(arg) == 0
      push!(ARGS,"test.json")
   end

   inputFile = arg
   start = max(length(inputFile)-4,1)
   if inputFile[start:end] != ".json"
      inputFile = inputFile*".json"
   end

   outfile = inputFile[1:end-5]*".jld"

   # Database naming convention:
   # ../data/collModel__NLaguerre__NLegendre__NSphere__NGauss.jld
   # e.g.: ../data/hardsphere__3__3__38__8.jld

   # Replace with:
   # Pkg.dir()*"lightningboltz/data/"
   # Replace with online database once parallel

   ### Process inputs
   return processInputs_json(inputFile), outfile
end

function runCase(p::paramData)

   datadir = "$(@__DIR__)/../data/"

   N = p.NLaguerre*p.NLegendre^2

   collMatrix = zeros(N,N,N)
   linCollMatrix = zeros(N,N,p.Nz)
   massMatrix=zeros(N,N)
   EfieldMatrix=zeros(N,N)
   sourceVector = zeros(N,p.Nz)
   sinkMatrix = zeros(N,N,p.Nz)
   fi = zeros(N,p.Nz)

   calculate_v_arrays(p)

   massMatrix, EfieldMatrix, transportMatrix_z = buildMassMatrix(p)

   Uz = zeros(N)
   Rz = zeros(size(massMatrix))
   if p.Nz > 1
      Uz,Rz= decomposeTransportMatrix(transportMatrix_z, massMatrix)
   end

   dtmax = (p.Lz/p.Nz)/maximum(abs.(Uz))
   println("dtmax = $dtmax")
   vref = sqrt(p.Tref/p.mass)
   Kn = 1.0/(p.Lz*sum(p.initDens)*p.sigref)
   println("Kn = $Kn")

   Minv = invertWithPrecision(massMatrix)

   # Get plasma properties
   dz = p.Lz/p.Nz
   if p.Nz > 1
      z_grid = collect(range(0.5*dz,stop=p.Lz-0.5*dz,length=p.Nz))
   else
      z_grid = [0.0]
   end

   ne = zeros(p.Nz)
   Te = zeros(p.Nz)
   Ti = zeros(p.Nz)
   upar = zeros(p.Nz)

   if p.Nz == 1 
#      ne[:] .= 1.0e19
#      Te[:] .= 2.0e-19
#      Ti[:] .= deepcopy(Te)
      ne[:] .= sum(p.initDens)
      Te[:] .= p.Tref
      Ti[:] .= p.Tref
   end

   if p.backgroundModel == "uniformbenchmark"
      ne = 5.0e19*ones(p.Nz)
      Te = 10.0*1.602e-19*ones(p.Nz)
      Ti = 20.0*1.602e-19*ones(p.Nz)
      upar= zeros(p.Nz)
   elseif p.backgroundModel == "uniformel"
      ne = 1.0e20*ones(p.Nz)
      Te = 0.8*1.602e-19*ones(p.Nz)
      Ti = 2.0*1.602e-19*ones(p.Nz)
      upar= zeros(p.Nz)
   elseif p.backgroundModel == "0dtest"
      ne = 1.0e22
      Te = 1.0*1.602e-19
      Ti = 1.0*1.602e-19
      upar = 0.0
   elseif p.backgroundModel == "d2elbenchmark"
      x = z_grid
      ne = 1.0e20*ones(length(x))

      d = 0.001
      x0 = 0.005
      Te_up = 5.0*1.602e-19
      Te_w = 0.8*1.602e-19
      Ti_up = 1.0*1.602e-19
      Ti_w = 1.0*1.602e-19

      Te = Te_w .+ 0.5*(Te_up-Te_w)*(1.0 .+ tanh.((x.-x0)/d))
      Ti = Ti_w .+ 0.5*(Ti_up-Ti_w)*(1.0 .+ tanh.((x.-x0)/d))

      #cs = sqrt.(Te_w/p.mass)
      #upar= cs*(x.-p.Lz)/p.Lz
      upar= zeros(length(x))
   elseif p.backgroundModel == "d2benchmark"
      x = z_grid .- 0.5*p.Lz
      ne = 5.0e19*ones(length(x))

      TiTe = 2.0
      delta = 0.1
      Te_up = 10.0*1.602e-19
      Ti_up = TiTe*Te_up
      Tw = p.bndy_z_temp[1]

      Te = Tw .+ (Te_up-Tw)*tanh.( (1.0/delta)*(1.0 .- abs.( 1.0 .- 2*z_grid/p.Lz) ) )
      Ti = Tw .+ (Ti_up-Tw)*tanh.( (1.0/delta)*(1.0 .- abs.( 1.0 .- 2*z_grid/p.Lz) ) )
#
      cs = sqrt.(Tw/p.mass)
      #upar= cs*x/(0.5*p.Lz)
      upar= zeros(length(x))
   end

   # Spectral expansion of ion distribution
   for iz in 1:p.Nz
      fi[:,iz] .= expand_fM_local(p,ne[iz],[0.0,0.0,upar[iz]],Ti[iz])
      fi[:,iz] .= Minv*fi[:,iz]
   #   n0,u0,T0,E0,M0,r0,s0,q0,dummy = calculate_moments(fi[:,iz],p)
   end

   for icoll in 1:length(p.collModel)
      matrixFile = datadir*getMatrixID(p,icoll)*".jld"

      matrixIsLocal=false
      if isfile(matrixFile) && p.useLocalMatrixFile
         matrixIsLocal=true
      end

      if ~matrixIsLocal && p.downloadMatrixFile
         try run(`wget "w3.pppl.gov/~gwilkie/lbdata/"$(getMatrixID(p)*".jld") -O $matrixFile`); matrixIsLocal=true
         catch
            if isfile(matrixFile) 
               run(`rm $matrixFile`)
            end
            matrixIsLocal=false
         end
      end

      if isfile(matrixFile) && p.useLocalMatrixFile
         matrixIsLocal=true
      end

      if matrixIsLocal
         Ctemp = p.collParams[icoll]*load(matrixFile,"collMatrix")
      else
         if occursin("lxcat",p.collModel[icoll])
            Ctemp = buildStationaryCollMatrix(p,icoll)
         elseif occursin("elec",p.collModel[icoll])
            Ctemp = buildStationaryCollMatrix(p,icoll)
         else
            Ctemp = buildCollMatrix(p,icoll)
         end
         if p.saveMatrixFile
            save(matrixFile,"collMatrix",Ctemp)
         end
      end

      # For simple collision models, a factor representing the reference cross section multiplies the collision operator.
      if occursin("maxwPseudo",p.collModel[icoll]) || occursin("hardsphere",p.collModel[icoll])
         Ctemp = Ctemp * p.sigref
      end

      if icoll == 1  # First listed collision operator is nonlinear
         collMatrix = p.collParams[icoll]*Ctemp
      else # Rest are linear 
         for iz in 1:p.Nz
            for i in 1:N
               if occursin("lxcat",p.collModel[icoll])
                  linCollMatrix[i,:,iz] += p.collParams[icoll]*p.initDens[1]*Ctemp[:,i]
               elseif occursin("elec",p.collModel[icoll])
                  linCollMatrix[i,:,iz] += p.collParams[icoll]*p.initDens[1]*Ctemp[:,i]
               else
                  linCollMatrix[i,:,iz] += p.collParams[icoll]*Ctemp[:,:,i]*fi[:,iz]
               end
            end
         end
      end
   end

   if p.Nz > 1
#      f_z_low, f_z_high = calculate_boundary_f(p,Minv)
      f_z_low, f_z_high = calculate_boundary_source(p)
      f_z_low = Minv*f_z_low
      f_z_high = Minv*f_z_high
      g_z_low = Uz.*(Rz\f_z_low)
      g_z_high = Uz.*(Rz\f_z_high) 
#      g_z_high = -Uz.*(Rz\f_z_high) # Negative sign is because this acts as a flux in the negative direction at the rightmost boundary

   end

   recombrate = 0.0

   for isource in 1:length(p.sourceModel)

      if p.sourceModel[isource] == "benchmark"
         for iz in 1:p.Nz
            x = z_grid[iz] - 0.5*p.Lz
            x0 = 1.0
            S0 = 1.0e23
            flr = 0.001
            S = S0*((sech((sign(x)*0.5*p.Lz-x)/x0))^2 + flr)
            sourceVector[:,iz] = S*buildMaxwSource(p,2.0*1.602e-19)
         end
      elseif p.sourceModel[isource][1:7] == "degas2_"
         
         reaction_str = p.sourceModel[isource][8:end]
         filename = degas2dir*"/"*reaction_str*".nc"
         func = get_degas2_data(filename,"reaction_rate")
         for iz in 1:p.Nz
            logn = log(ne[iz])
            logT = log(Te[iz])
            rate = exp(func(logT,logn))
            sourceVector[:,iz] .= rate*ne[iz]*massMatrix*fi[:,iz]
#            sourceVector[:,iz] .= rate*ne[iz]*fi[:,iz]
            if iz == 256
               recombrate = rate
            end
         end
      else
         error("Could not find sourceModel = $(p.sourceModel[isource]) \n")
      end
   end

   ionizrate = 0.0
   for isink in 1:length(p.sinkModel)

      sinkfactor=0.0
      if p.sinkModel[isink] == "auto"
         beta = 13.6/p.Tref
         sinkfactor = p.sigref*3.92*beta^1.5/(beta+0.35)
         for iz in 1:p.Nz
            sinkMatrix[:,:,iz] = massMatrix*sinkfactor
         end
      elseif p.sinkModel[isink][1:5] == "const"
         try sinkfactor = float(p.sinkModel[isink][7:end])
         catch
            error("Could not parse parameter from sinkModel = $(p.sinkModel[isink])")
         end
         for iz in 1:p.Nz
            sinkMatrix[:,:,iz] = massMatrix*sinkfactor
         end
      elseif p.sinkModel[isink][1:7] == "degas2_"
         reaction_str = p.sinkModel[isink][8:end]
         filename = degas2dir*"/"*reaction_str*".nc"
         func = get_degas2_data(filename,"reaction_rate")
         for iz in 1:p.Nz
            logn = log(ne[iz])
            logT = log(Te[iz])
            rate = exp(func(logT,logn))
            if iz == 256
               ionizrate = rate
               println(ionizrate)
               println(recombrate)
               println(ne[1]*recombrate/ionizrate)
            end
            sinkMatrix[:,:,iz] = rate*ne[iz]*massMatrix
         end
      else
         error("Could not find sinkModel = $(p.sinkModel[isink]) \n")
      end
   end

#   if ionizrate != 0.0 && recombrate != 0.0
#      println("Rescaling right density to match expected steady state from source and sink rates")
#      p.bndy_z_dens[2] = (recombrate/ionizrate)* ne[end]
#   end

   fcoeffs = initialize(Minv,p)

   fOut = zeros(Float64,(p.NLaguerre*p.NLegendre^2,p.Nz,min(p.Ntime,p.Nout)))
   tOut = zeros(p.Nout)
   fOut[:,:,1] = fcoeffs
   iOut = 1

   ### Advance in time, save up to Nout steps
   prog = Progress(p.Ntime,0.5,"Advancing in time...")
   first = true
   for it in 1:p.Ntime
      E_inst = p.Ez*cos(2*pi*p.Efreq*(it-1)*p.dt)
      if (p.Nz>1)

         fcoeffs = advanceTimestep_1d(fcoeffs,p.dt,p.timeMethod,p.nNewtonIter,massMatrix,Minv,E_inst*EfieldMatrix,collMatrix,linCollMatrix,sinkMatrix,sourceVector,transportMatrix_z,Rz,Uz,g_z_low,g_z_high,first)
#         fcoeffs = advanceTimestep_1d_LF(fcoeffs,p.dt,p.timeMethod,p.nNewtonIter,massMatrix,Minv,E_inst*EfieldMatrix,collMatrix,linCollMatrix,sinkMatrix,sourceVector,transportMatrix_z,f_z_low,f_z_high)
      else
         fcoeffs = advanceTimestep_0d(fcoeffs,p.dt,p.timeMethod,p.nNewtonIter,massMatrix,Minv,E_inst*EfieldMatrix,collMatrix,linCollMatrix[:,:,1],sinkMatrix[:,:,1],sourceVector)
      end

      if isnan(sum(fcoeffs))
         error("Coefficients became NaN at timestep $it.")
      end
      
      if in( it, round.(Int64,range(1,p.Ntime, length=p.Nout-1)) )
         iOut += 1
         fOut[:,:,iOut] = fcoeffs
         tOut[iOut] = (it-1)*p.dt
         if p.Nz > 1
            plot_1d_status(fcoeffs,p,massMatrix,iOut)
         end

      end
      first = false
      next!(prog)

   end

   return tOut, fOut, collMatrix, massMatrix, Minv
end

